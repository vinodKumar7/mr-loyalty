//
//  ProductListParser.h
//  GrabItNow
//
//  Created by MyRewards on 12/23/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@protocol ProductListXMLParserDelegate;

@interface ProductListParser : NSObject <NSXMLParserDelegate> {
    __unsafe_unretained id <ProductListXMLParserDelegate> delegate;
}

@property (unsafe_unretained) id <ProductListXMLParserDelegate> delegate;

@end


@protocol ProductListXMLParserDelegate <NSObject>
- (void) parsingProductListFinished:(NSArray *) productsList;
- (void) parsingProductListXMLFailed;
@end