//
//  OffersViewController.h
//  GrabItNow
//
//  Created by MyRewards on 12/1/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffersViewController : UIViewController

@property (nonatomic, strong) IBOutlet UITableView *offersTable;


@end
