//
//  FreebiesViewController.m
//  CoffeeApp
//
//  Created by vairat on 23/04/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import "FreebiesViewController.h"
#import "AppDelegate.h"
#import "LoyalityTableViewCell.h"
#import "ASIFormDataRequest.h"
#import "ProductListParser.h"
#import "Product.h"
#import "KeyboardViewController.h"


@interface FreebiesViewController ()
{
    AppDelegate *appDelegate;
    ASIFormDataRequest *freebiesListRequest;
    ProductListParser  *productsXMLParser;
    UIImage *fetchedProductImage;
    
    Product *currentProduct;
    
    NSMutableDictionary *staticImageDictionary;

}
@property(nonatomic, strong)UIImage *fetchedProductImage;
@end

@implementation FreebiesViewController
@synthesize freebiesList,freebiesTableView,fetchedProductImage;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    freebiesTableView.delegate = self;
    freebiesTableView.dataSource = self;
    
    
}
-(void)viewWillAppear:(BOOL)animated
{
    
    [appDelegate.homeViewController hideBackButon:YES];
    [appDelegate.homeViewController setHeaderTitle:@"MY FREEBIES"];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    
    
    NSString *urlString = [NSString stringWithFormat:@"http://184.107.152.53/newapp/freebies_list.php"];
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"url is %@", url);
    //  NSLog(@"url is %@ product_ID is %@ device ID id %@",url,product_ID,deviceID);
    freebiesListRequest = [[ASIFormDataRequest alloc] initWithURL:url];
    [freebiesListRequest setPostValue:@"2A240C91-C641-45CD-A8B7-6F96B3498A9D" forKey:@"uid"];
    // [freebiesRequest setPostValue:product_id forKey:@"pid"];
    [freebiesListRequest setPostValue:@"24" forKey:@"cid"];
    //  [freebiesRequest setPostValue:@"1"forKey:@"quantity"];
    // [freebiesRequest setPostValue:self.collectionViewManager.OProduct.productDesciption forKey:@"pdesc"];
    // [freebiesRequest setPostValue:@"update" forKey:@"function"];
    [freebiesListRequest setDelegate:self];
    [freebiesListRequest startAsynchronous];
    
    freebiesTableView.frame=self.view.frame;
    //freebiesTableView.contentSize=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [freebiesTableView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];

}


-(BOOL)prefersStatusBarHidden{

    return YES;
}
#pragma mark- TableView DataSource and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [freebiesList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"loyaltyDetailCell";
    
    LoyalityTableViewCell *cell;
    
    if (cell == nil)
        cell = (LoyalityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // http://184.107.152.53/files/product_image/1029846.jpg
    cell.tag = indexPath.row;
    currentProduct = [freebiesList objectAtIndex:indexPath.row];
    
    cell.lblTitle.text =currentProduct.productName;
    cell.lblSubTitle.text =currentProduct.productOffer;
    cell.lblQuantity.text=[NSString stringWithFormat:@"Quantity :%@",currentProduct.quantity];
    
    
    
    NSString *imageUrl = [NSString stringWithFormat:@"%@", currentProduct.carouselImgLink];
    // Here we either load from the web or we cache it...
    UIImage *image = [self imageNamed:imageUrl cache:YES];
    
    NSLog(@"productImage is %@ ",image);
    // NSLog(@"staticImageDictionary is %@ ",staticImageDictionary);
    cell.product_ImageView.image = nil;
    cell.product_ImageView.image=image;

    /*dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:currentProduct.carouselImgLink]];
        
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        //NSLog(@"");
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (cell.tag == indexPath.row) {
                    cell.product_ImageView.image = image;
                    //[productImagesArray addObject:image];
                    // NSLog(@"count of product image is %d ",productImagesArray.count);
                    [cell setNeedsLayout];
                }
            });
        }
    });*/
    
    
    int rowModuleNo = indexPath.row % 2;
    
    if (rowModuleNo == 0)
        cell.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    
    else if (rowModuleNo == 1)
        cell.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];

    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    KeyboardViewController *kvc = [self.storyboard   instantiateViewControllerWithIdentifier:@"KeyboardViewController"];
    
    currentProduct = [freebiesList objectAtIndex:indexPath.row];
    
    kvc.product_ID    = currentProduct.productId;
    LoyalityTableViewCell *cell = (LoyalityTableViewCell *)[freebiesTableView cellForRowAtIndexPath:indexPath];
    self.fetchedProductImage = cell.product_ImageView.image;
    kvc.product_Image = self.fetchedProductImage;
    kvc.product_Name  = currentProduct.productName;
    kvc.product_Offer = currentProduct.productOffer;
    kvc.deviceID = @"2A240C91-C641-45CD-A8B7-6F96B3498A9D";
    kvc.isFromFreebies=YES;
    //        kvc.isLastStamp = isLastStamp;
    kvc.redeemPassword = currentProduct.redeemPassword;
    
    NSLog(@"product_ID is %@",currentProduct.productId);
    
    [self.navigationController pushViewController:kvc animated:NO];
}


#pragma mark- storing images in NSdictionary

- (UIImage*)imageNamed:(NSString*)imageNamed cache:(BOOL)cache
{
    UIImage* retImage = [staticImageDictionary objectForKey:imageNamed];
    
    if (![self validateUrl:imageNamed])
    {
        
        retImage= [UIImage imageNamed:@"no_image_Available"];
        
    }
    else
    {
        //UIImage* retImage = [staticImageDictionary objectForKey:imageNamed];
        if (retImage == nil)
        {
            retImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageNamed]]];
            if (cache)
            {
                if (staticImageDictionary == nil)
                    staticImageDictionary = [NSMutableDictionary new];
                
                [staticImageDictionary setObject:retImage forKey:imageNamed];
            }
        }
    }
    return retImage;
}

- (BOOL) validateUrl: (NSString *) candidate
{
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- RequestFinished and RequestFailed

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSLog(@"=======>> Product:: %@",[request responseString]);
   
    NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
    productsXMLParser = [[ProductListParser alloc] init];
    productsXMLParser.delegate = self;
    productsParser.delegate = productsXMLParser;
    [productsParser parse];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    NSLog(@"=======>> Product:: %@",[request responseString]);
    
    UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                         message:@"Server Busy"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [alert_view show];
    
    
}
#pragma mark- Calling Parsing Methods
- (void)parsingProductListFinished:(NSArray *)prodcutsListLocal
{
   // NSLog(@"product name %@",prodcutsListLocal);
    Product *pro = [prodcutsListLocal objectAtIndex:0];
//    
    NSLog(@"redeem password =====> %@",pro.redeemPassword);
    
    if (!freebiesList)
        
        freebiesList = [[NSMutableArray alloc] initWithArray:prodcutsListLocal];
    
    else
    {
        [freebiesList removeAllObjects];
        [freebiesList addObjectsFromArray:prodcutsListLocal];
    }
    
    [freebiesTableView reloadData];
}
#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    
   /* if ([segue.identifier isEqualToString:@"KeyBoardVC"])
    {
        NSIndexPath *indexPath = [self.freebiesTableView indexPathForSelectedRow];
        
        LoyalityTableViewCell *cell = (LoyalityTableViewCell *)[freebiesTableView cellForRowAtIndexPath:indexPath];
        self.fetchedProductImage = cell.product_ImageView.image;
       Product *pro = [freebiesList objectAtIndex:indexPath.row];
        KeyboardViewController *kvc = segue.destinationViewController;
        kvc.product_ID    = pro.productId;
       kvc.product_Image = self.fetchedProductImage;
        kvc.product_Name  = pro.productName;
        kvc.deviceID = @"2A240C91-C641-45CD-A8B7-6F96B3498A9D";
        kvc.isFromFreebies=YES;
//        kvc.isLastStamp = isLastStamp;
        kvc.redeemPassword = pro.redeemPassword;
//        kvc.multipleRedeemCount = self.collectionViewManager.selectedStampCount;
//        NSLog(@"self.collectionViewManager.selectedStampCount:: %@",self.collectionViewManager.selectedStampCount);
//        controlCameFromKeyboardView = YES;
    }*/


}
- (void)parsingProductListXMLFailed
{
    NSLog(@"Product list updated failed ");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
