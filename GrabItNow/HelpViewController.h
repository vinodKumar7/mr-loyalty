//
//  HelpViewController.h
//  GrabItNow
//
//  Created by Venkat Sasi Allamraju on 05/03/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> 

@interface HelpViewController : UIViewController 
{
    
}
@property (unsafe_unretained, nonatomic) IBOutlet UIPageControl *coarouselPage_Control;


@property (nonatomic, strong) IBOutlet UIView *activityView;
@property (nonatomic, strong) IBOutlet UIImageView *screens_ImageView;
- (UIViewController *) controllerAtIndex:(NSInteger) index ;
@end
