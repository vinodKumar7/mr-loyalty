//
//  RedeemXMLparser.h
//  CWE
//
//  Created by vairat on 21/08/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/Foundation.h>
#import "Redeem.h"
@protocol RedeemXMLparserDelegate;

@interface RedeemXMLparser : NSObject <NSXMLParserDelegate>

{
    __unsafe_unretained id <RedeemXMLparserDelegate> delegate;

}
@property (unsafe_unretained) id <RedeemXMLparserDelegate> delegate;

@end
@protocol RedeemXMLparserDelegate <NSObject>
- (void) parsingcountFinished:(Redeem *) redeemObj;
- (void) RedeemXMLparsingFailed;
@end


