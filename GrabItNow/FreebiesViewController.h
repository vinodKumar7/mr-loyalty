//
//  FreebiesViewController.h
//  CoffeeApp
//
//  Created by vairat on 23/04/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductListParser.h"

@interface FreebiesViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, ProductListXMLParserDelegate>

@property (strong, nonatomic) IBOutlet UITableView *freebiesTableView;
@property (strong, nonatomic) NSMutableArray *freebiesList;

@end
