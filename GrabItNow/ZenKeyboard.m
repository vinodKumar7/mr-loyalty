//
//  ZenKeyboard.m
//  ZenKeyboard
//
//  Created by Kevin Nick on 2012-11-9.
//  Copyright (c) 2012年 com.zen. All rights reserved.
//

#import "ZenKeyboard.h"
#define kMaxNumber                       100000
@interface ZenKeyboard()

@property (nonatomic,assign) id<UITextInput> textInputDelegate;

@end;

@implementation ZenKeyboard

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSLog(@"frame.size.width is %f ",frame.size.width);
      //  UIImageView *keyboardBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"KeyboardBackgroundTextured"]];
      //  UIImageView *keyboardGridLines = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"KeyboardNumericEntryViewGridLinesTextured"]];
        UIView *accessoryView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, 60)];
        UILabel *merchantLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, 50)];
        merchantLabel.tag=666;
        merchantLabel.numberOfLines=2;
        merchantLabel.textAlignment=NSTextAlignmentCenter;
        merchantLabel.backgroundColor=[UIColor clearColor];
        
        merchantLabel.text=@" Merchant must enter their PIN for you to redeem this offer";
        merchantLabel.textColor=[UIColor whiteColor];
        //[self addSubview:keyboardBackground];
       // [self addSubview:keyboardGridLines];
        [self addSubview:[self addNumericKeyWithTitle:@"1" frame:CGRectMake(0, 61, frame.size.width/3 - 3, KEYBOARD_NUMERIC_KEY_HEIGHT)]];
        [self addSubview:[self addNumericKeyWithTitle:@"2" frame:CGRectMake(frame.size.width/3  - 2, 61, frame.size.width/3 , KEYBOARD_NUMERIC_KEY_HEIGHT)]];
        [self addSubview:[self addNumericKeyWithTitle:@"3" frame:CGRectMake(frame.size.width/3  * 2 - 1, 61, frame.size.width/3  - 1, KEYBOARD_NUMERIC_KEY_HEIGHT)]];
        
        [self addSubview:[self addNumericKeyWithTitle:@"4" frame:CGRectMake(0, KEYBOARD_NUMERIC_KEY_HEIGHT + 62, frame.size.width/3  - 3, KEYBOARD_NUMERIC_KEY_HEIGHT)]];
        [self addSubview:[self addNumericKeyWithTitle:@"5" frame:CGRectMake(frame.size.width/3  - 2, KEYBOARD_NUMERIC_KEY_HEIGHT + 62, frame.size.width/3 , KEYBOARD_NUMERIC_KEY_HEIGHT)]];
        [self addSubview:[self addNumericKeyWithTitle:@"6" frame:CGRectMake(frame.size.width/3  * 2 - 1, KEYBOARD_NUMERIC_KEY_HEIGHT + 62, frame.size.width/3  - 1, KEYBOARD_NUMERIC_KEY_HEIGHT)]];
        
        [self addSubview:[self addNumericKeyWithTitle:@"7" frame:CGRectMake(0, KEYBOARD_NUMERIC_KEY_HEIGHT * 2 + 63, frame.size.width/3  - 3, KEYBOARD_NUMERIC_KEY_HEIGHT)]];
        [self addSubview:[self addNumericKeyWithTitle:@"8" frame:CGRectMake(frame.size.width/3  - 2, KEYBOARD_NUMERIC_KEY_HEIGHT * 2 + 63, frame.size.width/3  , KEYBOARD_NUMERIC_KEY_HEIGHT)]];
        [self addSubview:[self addNumericKeyWithTitle:@"9" frame:CGRectMake(frame.size.width/3  * 2 - 1, KEYBOARD_NUMERIC_KEY_HEIGHT * 2 + 63, frame.size.width/3 , KEYBOARD_NUMERIC_KEY_HEIGHT)]];
        
        [self addSubview:[self addInfoKeyWithFrame:CGRectMake(0, KEYBOARD_NUMERIC_KEY_HEIGHT * 3 + 64, frame.size.width/3  - 3, KEYBOARD_NUMERIC_KEY_HEIGHT)]];
        
        // CGRectMake(0, KEYBOARD_NUMERIC_KEY_HEIGHT * 3 + 4, frame.size.width/3  - 3, KEYBOARD_NUMERIC_KEY_HEIGHT)
        [self addSubview:[self addNumericKeyWithTitle:@"0" frame:CGRectMake(frame.size.width/3  - 2, KEYBOARD_NUMERIC_KEY_HEIGHT * 3 + 64, frame.size.width/3 , KEYBOARD_NUMERIC_KEY_HEIGHT)]];
        [self addSubview:[self addBackspaceKeyWithFrame:CGRectMake(frame.size.width/3  * 2 - 1, KEYBOARD_NUMERIC_KEY_HEIGHT * 3 + 64, frame.size.width/3  - 3, KEYBOARD_NUMERIC_KEY_HEIGHT)]];
        accessoryView.backgroundColor=[UIColor colorWithRed:236.0/255.0 green:0.0/255.0 blue:140/255.0 alpha:1.0];
        UIImageView *checkImage;
        for (int i=0; i<=3; i++)
        {NSLog(@"i is %d x is %f accessory width is %f",i,frame.size.width/3*i,accessoryView.frame.size.width);
            checkImage =[[UIImageView alloc]initWithFrame:CGRectMake(frame.size.width/4*i+30,30,20,20)];
            checkImage.image=[UIImage imageNamed:@"unfill"];
            checkImage.tag=i+1;
            [accessoryView addSubview:checkImage];
        }
        merchantLabel.backgroundColor=[UIColor colorWithRed:236.0/255.0 green:0.0/255.0 blue:140/255.0 alpha:1.0];
        [accessoryView addSubview:merchantLabel];
        [self addSubview:accessoryView];
    }
    
    return self;
}

- (UIButton *)addNumericKeyWithTitle:(NSString *)title frame:(CGRect)frame {
    UIButton *button;
    if([title isEqualToString:@"i" ])
    {
        button = [UIButton buttonWithType:UIButtonTypeInfoLight];
    }
    else
        button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    [button setTitle:title forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont boldSystemFontOfSize:28.0]];
   // button.tintColor=[UIColor blackColor];
    button.backgroundColor=[UIColor whiteColor];
   // button.layer.borderColor=[UIColor blackColor].CGColor;
    //  button.layer.borderWidth=.2;
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    // [button setTitleShadowColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitleShadowColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [button.titleLabel setShadowOffset:CGSizeMake(0, -0.5)];
    
    //UIImage *buttonImage = [UIImage imageNamed:@"KeyboardNumericEntryKeyTextured"];
   // UIImage *buttonPressedImage = [UIImage imageNamed:@"KeyboardNumericEntryKeyPressedTextured"];
   // [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
   // [button setBackgroundImage:buttonPressedImage forState:UIControlStateHighlighted];
    [button addTarget:self action:@selector(pressNumericKey:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return button;
}

- (UIButton *)addBackspaceKeyWithFrame:(CGRect)frame {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    UIImage *buttonImage = [UIImage imageNamed:@"KeyboardNumericEntryKeyTextured"];
    UIImage *buttonPressedImage = [UIImage imageNamed:@"KeyboardNumericEntryKeyPressedTextured"];
    UIImage *image = [UIImage imageNamed:@"KeyboardNumericEntryKeyBackspaceGlyphTextured"];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake((buttonImage.size.width - image.size.width) / 2, (buttonImage.size.height - image.size.height) / 2, image.size.width, image.size.height)];
    imgView.image = image;
    [button addSubview:imgView];
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:buttonPressedImage forState:UIControlStateHighlighted];
    [button addTarget:self action:@selector(pressBackspaceKey) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}
- (UIButton *)addInfoKeyWithFrame:(CGRect)frame {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeInfoLight];
    button.frame = frame;
    UIImage *buttonImage = [UIImage imageNamed:@"KeyboardNumericEntryKeyTextured"];
    UIImage *buttonPressedImage = [UIImage imageNamed:@"KeyboardNumericEntryKeyPressedTextured"];
    UIImage *image = [UIImage imageNamed:@"KeyboardNumericEntryKeyBackspaceGlyphTextured"];
    //UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake((buttonImage.size.width - image.size.width) / 2, (buttonImage.size.height - image.size.height) / 2, image.size.width, image.size.height)];
    button.tintColor=[UIColor whiteColor];
    //imgView.image = image;
    // [button addSubview:imgView];
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:buttonPressedImage forState:UIControlStateHighlighted];
    [button addTarget:self action:@selector(InfoButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

- (void)setTextField:(UITextField *)textField {
    _textField = textField;
    _textField.inputView = self;
    self.textInputDelegate = _textField;
}

- (void)pressNumericKey:(UIButton *)button {
    NSString *keyText = button.titleLabel.text;
    int key = -1;
    
    if ([@"i" isEqualToString:keyText]) {
        key = 10;
    } else {
        key = [keyText intValue];
        // NSLog(@"_textField.text is %@  length %d",_textField.text,[_textField.text length]);
        NSLog(@"pressNumericKey %lu ",(unsigned long)[_textField.text length] );
        
        
        NSInteger x=[_textField.text length]+1;
        NSLog(@"x value is %ld",(long)x);
        
        [self loadingimagestoaccessoryViewwithcount:[_textField.text length]+1];
        
    }
    
    NSRange dot = [_textField.text rangeOfString:@"i"];
    
    switch (key) {
        case 10:
            if (dot.location == NSNotFound && _textField.text.length == 0) {
                //[self.textInputDelegate insertText:@"i"];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Working" message:@"hello" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            } else if (dot.location == NSNotFound) {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Working" message:@"hello" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                
            }
            
            break;
        default:
            if (kMaxNumber <= [[NSString stringWithFormat:@"%@%d", _textField.text, key] doubleValue]) {
                _textField.text = [NSString stringWithFormat:@"%d", kMaxNumber];
            } else if ([@"0.00" isEqualToString:_textField.text]) {
                _textField.text = [NSString stringWithFormat:@"%d", key];
            } else if (dot.location == NSNotFound || _textField.text.length <= dot.location + 2) {
                [self.textInputDelegate insertText:[NSString stringWithFormat:@"%d", key]];
            }
            
            break;
    }
}

- (void)pressBackspaceKey {
    NSLog(@"Back Space count is %lu ",(unsigned long)[_textField.text length] );
    [self loadingimagestoaccessoryViewwithcount:[_textField.text length]-1];
    if ([@"0." isEqualToString:_textField.text]) {
        _textField.text = @"";
        
        return;
    } else {
        NSLog(@"press Back texrt is %@",_textField.text);
        [self.textInputDelegate deleteBackward];
    }
}
- (void)InfoButtonPressed
{
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"How to Validate Offer" message:@"The Merchant must enter their own unique code to verify the customer's membership. if the merchant does not know the code , contact us immediately by sending an email to support@therewardsteam.com" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles: nil];
    [alert show];
    
    
}
-(void)loadingimagestoaccessoryViewwithcount:(NSInteger) count
{
    UIImageView *checkbox0 = (UIImageView *)[self viewWithTag:1];
    UIImageView *checkbox1 = (UIImageView *)[self viewWithTag:2];
    UIImageView *checkbox2 = (UIImageView *)[self viewWithTag:3];
    UIImageView *checkbox3 = (UIImageView *)[self viewWithTag:4];
    UILabel *merchantLabel = (UILabel *)[self viewWithTag:666];
    //NSLog(@"Switch count is %lu  text is %@ tag is %d",(unsigned long)
    if (count>0) {
//        merchantLabel.text=@"Enter PIN to verify";
//        CGRect labelframe=merchantLabel.frame;
//        labelframe.origin.y=-5;
//        merchantLabel.frame=labelframe;
         [self merchantlabelyorigin:-5 labelText:@"Enter PIN to verify" color:[UIColor clearColor]];
    }
    else
    {
      /*  merchantLabel.text=@"Merchant must enter their PIN for you to redeem this offer";
        CGRect labelframe=merchantLabel.frame;
        labelframe.origin.y=10;
        merchantLabel.frame=labelframe;*/
        [self merchantlabelyorigin:10 labelText:@"Merchant must enter their PIN for you to redeem this offer" color:[UIColor colorWithRed:236.0/255.0 green:0.0/255.0 blue:140/255.0 alpha:1.0]];
        
    }
    
    switch (count)
    {
        case 0:
        {
            merchantLabel.hidden=NO;
            // UIImageView *checkView = (UIImageView *)[self viewWithTag:1];
           // merchantLabel.text=@"Merchant must enter their PIN for you to redeem this offer";
//            CGRect labelframe=merchantLabel.frame;
//            labelframe.origin.y=10;
//            merchantLabel.frame=labelframe;
            checkbox0.image=[UIImage imageNamed:@"unfill"];
            checkbox1.image=[UIImage imageNamed:@"unfill"];
            checkbox2.image=[UIImage imageNamed:@"unfill"];
            checkbox3.image=[UIImage imageNamed:@"unfill"];
            //merchantLabel.backgroundColor=[UIColor colorWithRed:236.0/255.0 green:0.0/255.0 blue:140/255.0 alpha:1.0];
            [self merchantlabelyorigin:10 labelText:@"Merchant must enter their PIN for you to redeem this offer" color:[UIColor colorWithRed:236.0/255.0 green:0.0/255.0 blue:140/255.0 alpha:1.0]];
        }
            break;
        case 1:
        {  //  merchantLabel.hidden=YES;
            //            merchantLabel.text=@"Enter PIN to verify";
            //            CGRect labelframe=merchantLabel.frame;
            //            labelframe.origin.y=-5;
            //            merchantLabel.frame=labelframe;
//            merchantLabel.text=@"Enter PIN to verify";
//            CGRect labelframe=merchantLabel.frame;
//            labelframe.origin.y=-5;
//            merchantLabel.frame=labelframe;
//            merchantLabel.backgroundColor=[UIColor clearColor];
            
            [self merchantlabelyorigin:-5 labelText:@"Enter PIN to verify" color:[UIColor clearColor]];
            //MerchantmustLabel.text=@" Merchant must enter their PIN for you to redeem this offer";
            checkbox0.image=[UIImage imageNamed:@"fill"];
            checkbox1.image=[UIImage imageNamed:@"unfill"];
            checkbox2.image=[UIImage imageNamed:@"unfill"];
            checkbox3.image=[UIImage imageNamed:@"unfill"];
        }
            break;
        case 2:
        {
            checkbox0.image=[UIImage imageNamed:@"fill"];
            checkbox1.image=[UIImage imageNamed:@"fill"];
            checkbox2.image=[UIImage imageNamed:@"unfill"];
            checkbox3.image=[UIImage imageNamed:@"unfill"];
        }
            break;
        case 3:
        {
            checkbox0.image=[UIImage imageNamed:@"fill"];
            checkbox1.image=[UIImage imageNamed:@"fill"];
            checkbox2.image=[UIImage imageNamed:@"fill"];
            checkbox3.image=[UIImage imageNamed:@"unfill"];
        }
            break;
        case 4:
        {
            checkbox0.image=[UIImage imageNamed:@"fill"];
            checkbox1.image=[UIImage imageNamed:@"fill"];
            checkbox2.image=[UIImage imageNamed:@"fill"];
            checkbox3.image=[UIImage imageNamed:@"fill"];
            NSLog(@"Switch count case is 4");
            [self performSelector:@selector(resetImages) withObject:nil afterDelay:0.2];
        }
            break;
            
        default:
        {
            
        }
            break;
    }
    
}
-(void)resetImages
{
    NSLog(@"tag is %ld ",(long)_textField.tag);
    if (_textField.tag==100)
    {
        [self merchantlabelyorigin:10 labelText:@"validating.... " color:[UIColor colorWithRed:236.0/255.0 green:0.0/255.0 blue:140/255.0 alpha:1.0]];
        
    [self performSelector:@selector(delaying) withObject:nil afterDelay:0.6];
    }
    else
    { NSLog(@"tag is  else conditiu");
       
        _textField.tag=100;
       
        [self merchantlabelyorigin:10 labelText:@"Invalid PIN " color:[UIColor colorWithRed:236.0/255.0 green:0.0/255.0 blue:140/255.0 alpha:1.0]];
      
    }
    
    
}
-(void)delaying
{
[self loadingimagestoaccessoryViewwithcount:0];
}
-(void)merchantlabelyorigin:(NSInteger) yPosition labelText:(NSString *)title color:(UIColor *)color
{ UILabel *merchantLabel = (UILabel *)[self viewWithTag:666];
    merchantLabel.text=title;
    merchantLabel.backgroundColor=color;
    CGRect labelframe=merchantLabel.frame;
    labelframe.origin.y=yPosition;
    merchantLabel.frame=labelframe;


}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
