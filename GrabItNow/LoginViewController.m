//
//  LoginViewController.m
//  GrabItNow
//
//  Created by vairat on 29/04/15.
//  Copyright (c) 2015 MyRewards. All rights reserved.
//

#import "LoginViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "ASIFormDataRequest.h"
#import "User.h"
#import <Parse/Parse.h>
#import "DMLazyScrollView.h"
#import "JSON.h"


#define Status_XML_tag @"status"
#define Status_success_message @"SUCCESS"
#define Status_failure_message @"FAILURE"
#define kOFFSET_FOR_KEYBOARD 20

@interface LoginViewController ()
{
    AppDelegate *appDelegate;
    DMLazyScrollView* lazyScrollView;
    
    ASIFormDataRequest *logingRequest;
    ASIFormDataRequest *getUserRequest;
    
    NSXMLParser *parser;
    
    UserDataXMLParser *parserDelegate;
    
    NSString *userName;
    NSString *password;
    NSString *subDomainName;
    
    NSMutableArray*    viewControllerArray;
    NSArray *pageImages_Array;
    
    UIImageView *loginBackgroundImgView;
    UIImageView  *registerTextImgView;
    UIImageView  *logoImgView;
    
    UIView *baseView;
    UIView *registerBaseView;
    
    UIView *textFieldsBaseView;
    
    UITextField *usernameTextField;
    UITextField *passwordTextField;
    UITextField *currentTextField;
    UIButton *loginWithFbBtn;
    
    UITextField *firstNameTextField;
    UITextField *secondNameTextField;
    UITextField *emailTextField;
    UITextField *phNoTextField;
    
    int yCoordinates;
    int verticalSpace;
    
    int keyboardOffset;
}

@property(nonatomic,strong) NSArray *pageImages_Array;
@property(nonatomic,strong) UITextField *firstNameTextField;

- (BOOL) validateLogin;

@end

@implementation LoginViewController
@synthesize coarouselPage_Control, screens_ImageView;
@synthesize pageImages_Array;
@synthesize firstNameTextField;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.pageImages_Array = [[NSArray alloc]initWithObjects:@"LoyaltyCard1",@"LoyaltyCard2",@"LoyaltyCard3",@"background", nil];
    
    
    
    [self pageSetting];

}

- (void)viewWillAppear:(BOOL)animated
{
    
}

- (void)pageSetting
{
    // PREPARE PAGES
    NSUInteger numberOfPages = 4;
    viewControllerArray = [[NSMutableArray alloc] initWithCapacity:numberOfPages];
    for (NSUInteger k = 0; k < numberOfPages; k++) {
        [viewControllerArray addObject:[NSNull null]];
    }
    
    // PREPARE LAZY VIEW
    //    CGRect rect = CGRectMake(0, 0, self.view.frame.size.width, 548.0f);
    lazyScrollView = [[DMLazyScrollView alloc] initWithFrame:self.view.bounds];
    
    
    lazyScrollView.dataSource = ^(NSUInteger index) {
        return [self controllerAtIndex:index];
    };
    lazyScrollView.numberOfPages = numberOfPages;
    lazyScrollView.controlDelegate = self;
    lazyScrollView.userInteractionEnabled = YES;
    [self.view addSubview:lazyScrollView];
    
    
    [self.view bringSubviewToFront:self.coarouselPage_Control];
}

- (UIViewController *) controllerAtIndex:(NSInteger) index {
    
    
    if (index > viewControllerArray.count || index < 0) return nil;
    
    
    id res = [viewControllerArray objectAtIndex:index];
    if (res == [NSNull null]) {
        
        UIViewController *contr = [[UIViewController alloc] init];
        
        
        if (index != 3) {
            
            UIImageView *splashImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f,0.0f, self.view.bounds.size.width, self.view.bounds.size.height)];
            splashImgView.image = [UIImage imageNamed:[self.pageImages_Array objectAtIndex:index]];
            
            [contr.view addSubview:splashImgView];
        }
        else
        {
            

            [contr.view addSubview:[self loginPage]];
            
            
        }
        
        
        [viewControllerArray replaceObjectAtIndex:index withObject:contr];
        return contr;
    }
    return res;
}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView currentPageChanged:(NSInteger)currentPageIndex
{
    NSLog(@"currentPage %ld",(long)currentPageIndex);
    
    
    self.coarouselPage_Control.currentPage = currentPageIndex;
    
    
}

- (UIView *)loginPage
{
    
    
    baseView = [[UIView alloc]initWithFrame:self.view.bounds];
    
    loginBackgroundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f,0.0f, self.view.bounds.size.width, self.view.bounds.size.height)];
    loginBackgroundImgView.image = [UIImage imageNamed:[self.pageImages_Array objectAtIndex:3]];
    

    [baseView addSubview:loginBackgroundImgView];
    
    if ([ [ UIScreen mainScreen ] bounds ].size.height == 480.0) {
        textFieldsBaseView = [[UIView alloc]initWithFrame:CGRectMake(20, self.view.bounds.size.height/2, self.view.bounds.size.width - 40, self.view.bounds.size.height/3.5)];
        
        yCoordinates = 20;
        verticalSpace = 30;
    }
    else
    {
        textFieldsBaseView = [[UIView alloc]initWithFrame:CGRectMake(20, self.view.bounds.size.height/1.8, self.view.bounds.size.width - 40, self.view.bounds.size.height/3)];
        yCoordinates = 50;
        verticalSpace = 60;
    }
    
    textFieldsBaseView.backgroundColor = [UIColor colorWithRed:165/255.0 green:223/255.0 blue:232/255.0 alpha:1.0];
    
    
    logoImgView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width/3.5, yCoordinates, 130, 130)];
    logoImgView.image = [UIImage imageNamed:@"mrLoginLogo.png"];
    [loginBackgroundImgView addSubview:logoImgView];
    
    registerTextImgView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width/4.5, logoImgView.bounds.origin.y + logoImgView.bounds.size.height + verticalSpace, 180, 50)];
    registerTextImgView.image = [UIImage imageNamed:@"registertext.png"];
    [loginBackgroundImgView addSubview:registerTextImgView];
    
    loginWithFbBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    loginWithFbBtn.frame = CGRectMake(20, registerTextImgView.frame.origin.y + 70 , self.view.bounds.size.width - 40, 45);
    [loginWithFbBtn setBackgroundImage:[UIImage imageNamed:@"loginwithfb.png"] forState:UIControlStateNormal];
    [loginWithFbBtn addTarget:self action:@selector(loginWithFacebookPressed:) forControlEvents:UIControlEventTouchUpInside];
    [baseView addSubview:loginWithFbBtn];

    

    
    usernameTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 20, textFieldsBaseView.bounds.size.width - 20, textFieldsBaseView.bounds.size.height/4.5)];
    [usernameTextField setFont:[UIFont fontWithName:@"MyriadPro-Light" size:21]];
    usernameTextField.delegate = self;
    usernameTextField.userInteractionEnabled = YES;
    usernameTextField.placeholder = @"   User name";
    usernameTextField.returnKeyType = UIReturnKeyNext;
    usernameTextField.backgroundColor = [UIColor whiteColor];
    [textFieldsBaseView addSubview:usernameTextField];
    
    passwordTextField = [[UITextField alloc]initWithFrame:CGRectMake(10,usernameTextField.bounds.size.height +30, textFieldsBaseView.bounds.size.width - 20, textFieldsBaseView.bounds.size.height/4.5)];
    [passwordTextField setFont:[UIFont fontWithName:@"MyriadPro-Light" size:21]];
    passwordTextField.delegate = self;
    passwordTextField.placeholder = @"   Password";
    passwordTextField.secureTextEntry = YES;
    passwordTextField.backgroundColor = [UIColor whiteColor];
    [textFieldsBaseView addSubview:passwordTextField];
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    loginButton.frame = CGRectMake(10, passwordTextField.bounds.size.height*2 +40 , passwordTextField.bounds.size.width/2 -5, textFieldsBaseView.bounds.size.height/4);
    [loginButton setBackgroundImage:[UIImage imageNamed:@"loginBtn.png"] forState:UIControlStateNormal];
    [loginButton addTarget:self action:@selector(loginBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [textFieldsBaseView addSubview:loginButton];
    
    UIButton *registerButton = [[UIButton alloc]initWithFrame:CGRectMake(loginButton.bounds.size.width +20, passwordTextField.bounds.size.height*2 +40 , passwordTextField.bounds.size.width/2 -5, textFieldsBaseView.bounds.size.height/4)];
    [registerButton setBackgroundColor:[UIColor colorWithRed:75/255.0 green:193/255.0 blue:214/255.0 alpha:1.0]];
    [registerButton setTitle:@"Register" forState:UIControlStateNormal];
    registerButton.titleLabel.font = [UIFont fontWithName:@"MyriadPro-Light" size:21];
    
    
    [registerButton addTarget:self action:@selector(registerBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [textFieldsBaseView addSubview:registerButton];
    
    
    [baseView addSubview:textFieldsBaseView];
    
    return baseView;
}

- (UIView *)registrationPage
{
    registerBaseView = [[UIView alloc]initWithFrame:CGRectMake(25, logoImgView.frame.origin.y + logoImgView.frame.size.height +10, self.view.bounds.size.width - 50, self.view.bounds.size.height/1.7)];
    registerBaseView.backgroundColor = [UIColor colorWithRed:165/255.0 green:223/255.0 blue:232/255.0 alpha:1.0];
    
    UILabel *registrationLbl = [[UILabel alloc]initWithFrame:CGRectMake(20, 15, registerBaseView.bounds.size.width -40, registerBaseView.bounds.size.height/7)];
    [registrationLbl setFont:[UIFont fontWithName:@"MyriadPro-Light" size:21]];
    registrationLbl.text = @"Registration";
    registrationLbl.textColor = [UIColor whiteColor];
    registrationLbl.textAlignment = NSTextAlignmentCenter;
    registrationLbl.layer.borderWidth = 1.0;
    registrationLbl.layer.borderColor = [UIColor whiteColor].CGColor;
    registrationLbl.backgroundColor = [UIColor colorWithRed:52/255.0 green:184/255.0 blue:204/255.0 alpha:1.0];
    [registerBaseView addSubview:registrationLbl];
    
    firstNameTextField = [[UITextField alloc]initWithFrame:CGRectMake(20, registrationLbl.frame.origin.y + registrationLbl.bounds.size.height + 7, registerBaseView.bounds.size.width - 40, registerBaseView.bounds.size.height/8)];
    [firstNameTextField setFont:[UIFont fontWithName:@"MyriadPro-Light" size:19]];
    firstNameTextField.delegate = self;
    firstNameTextField.placeholder = @"   First Name";
    firstNameTextField.returnKeyType = UIReturnKeyNext;
    firstNameTextField.backgroundColor = [UIColor whiteColor];
    [registerBaseView addSubview:firstNameTextField];
    
    secondNameTextField = [[UITextField alloc]initWithFrame:CGRectMake(20,firstNameTextField.frame.origin.y + firstNameTextField.bounds.size.height + 7, registerBaseView.bounds.size.width - 40, registerBaseView.bounds.size.height/8)];
    
    [secondNameTextField setFont:[UIFont fontWithName:@"MyriadPro-Light" size:19]];
    secondNameTextField.delegate = self;
    secondNameTextField.placeholder = @"   Second Name";
    secondNameTextField.returnKeyType = UIReturnKeyNext;
    secondNameTextField.backgroundColor = [UIColor whiteColor];
    [registerBaseView addSubview:secondNameTextField];

    emailTextField = [[UITextField alloc]initWithFrame:CGRectMake(20,secondNameTextField.frame.origin.y + secondNameTextField.bounds.size.height + 7, registerBaseView.bounds.size.width - 40, registerBaseView.bounds.size.height/8)];
    
    [emailTextField setFont:[UIFont fontWithName:@"MyriadPro-Light" size:19]];
    emailTextField.delegate = self;
    emailTextField.placeholder = @"   Email";
    emailTextField.returnKeyType = UIReturnKeyNext;
    emailTextField.backgroundColor = [UIColor whiteColor];
    [registerBaseView addSubview:emailTextField];
    
    phNoTextField = [[UITextField alloc]initWithFrame:CGRectMake(20, emailTextField.frame.origin.y + emailTextField.bounds.size.height + 7, registerBaseView.bounds.size.width - 40, registerBaseView.bounds.size.height/8)];
    
    [phNoTextField setFont:[UIFont fontWithName:@"MyriadPro-Light" size:19]];
    phNoTextField.delegate = self;
    phNoTextField.placeholder = @"   Phone Number";
    phNoTextField.returnKeyType = UIReturnKeyDone;
    phNoTextField.backgroundColor = [UIColor whiteColor];
    [registerBaseView addSubview:phNoTextField];
    
    UIButton *registerLoginBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    registerLoginBtn.frame = CGRectMake(registerBaseView.bounds.size.width/3.5, phNoTextField.frame.origin.y + phNoTextField.bounds.size.height + 7 , phNoTextField.bounds.size.width/2 , registerBaseView.bounds.size.height/6);
    [registerLoginBtn setBackgroundImage:[UIImage imageNamed:@"loginBtn.png"] forState:UIControlStateNormal];
    [registerLoginBtn addTarget:self action:@selector(registerLoginBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [registerBaseView addSubview:registerLoginBtn];
    
    return registerBaseView;
}

- (IBAction)loginBtnPressed:(id)sender
{
    NSLog(@"btnClicked");
    
//    [appDelegate showhomeScreen];
    
    if ([usernameTextField isFirstResponder]) {
        [usernameTextField resignFirstResponder];
    }
    
    if ([passwordTextField isFirstResponder]) {
        [passwordTextField resignFirstResponder];
    }
    
//    if ([subDomainTextField isFirstResponder]) {
//        [subDomainTextField resignFirstResponder];
//    }
    
    
    [self readjustloginContainer];
    
    if (![self validateLogin]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fields Empty" message:@"All fields are mandatory. \nPlease fill all the fields." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [self performLoginWithUserName:usernameTextField.text password:passwordTextField.text domainName:@"www.myrewards.com.au"];
    
}

- (BOOL) validateLogin {
    
    BOOL success = YES;
    
    if (usernameTextField.text.length == 0 || passwordTextField.text.length == 0 ) {
        success = NO;
    }
    
    return success;
}


-(IBAction)registerBtnPressed:(id)sender
{
    NSLog(@"registerBtnPressed");
    
    textFieldsBaseView.hidden = YES;
    loginWithFbBtn.hidden = YES;
    registerTextImgView.hidden = YES;
    
    
    [baseView addSubview:[self registrationPage]];
}

- (IBAction)registerLoginBtnPressed:(id)sender
{
    
    registerBaseView.hidden = YES;
    textFieldsBaseView.hidden = NO;
    loginWithFbBtn.hidden = NO;
    registerTextImgView.hidden = NO;
    
}

- (IBAction)loginWithFacebookPressed:(id)sender
{
    NSLog(@"fb btnClicked");
    
    [self performSelector:@selector(publishPostWithDelegate:) withObject:nil afterDelay:0.2];
}

- (void) performLoginWithUserName:(NSString *) uName password:(NSString *) pWord domainName:(NSString *) domainName{
    
    if ([usernameTextField isFirstResponder]) {
        [usernameTextField resignFirstResponder];
    }
    
    if ([passwordTextField isFirstResponder]) {
        [passwordTextField resignFirstResponder];
    }
    
//    if ([subDomainTextField isFirstResponder]) {
//        [subDomainTextField resignFirstResponder];
//    }
    
    userName = uName;
    password = pWord;
    subDomainName = domainName;
    
    NSLog(@"subDomain is:%@",subDomainName);
    [self readjustloginContainer];
    
    
    
    
    /**** Here goes actual login checking code ****/
    
    NSString *urlString = [NSString stringWithFormat:@"%@login.php",URL_Prefix];
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIFormDataRequest *req = [[ASIFormDataRequest alloc] initWithURL:url];
    [req setPostValue:uName forKey:@"uname"]; // pwd, sub
    [req setPostValue:pWord forKey:@"pwd"];
    [req setPostValue:domainName forKey:@"sub"];
    [req setDelegate:self];
    [req startAsynchronous];
    logingRequest = req;
    
}

- (void) getuserDetails {
    
    //need to send user ID here as parameter.
    ASIFormDataRequest *req = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@get_user.php?uname=%@",URL_Prefix,userName]]];
    req.delegate = self;
    [req startAsynchronous];
    getUserRequest = req;
    
}

- (void) dismissLoginContainerWithUserDetails:(User *)uDetails password:(NSString *) pWord {
    
    uDetails.clientDomainName = subDomainName;
    
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    [appDelegate loginSuccessfulWithUserdetails:uDetails password:pWord];
    
}


#pragma mark - TextField Delegate Methods


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTextField = textField;
    
    if (textField == usernameTextField) {
        [self usernameTextFieldDidBegin];
    }
    else if (textField == passwordTextField) {
        [self passwordTextFieldDidBegin];
    }
    
    else if (textField == firstNameTextField)
    {
        keyboardOffset = 20;
        [self setViewMovedUp:YES  TextField:textField];
    }
    else if (textField == secondNameTextField)
    {
        keyboardOffset = 50;
        [self setViewMovedUp:YES  TextField:textField];
    }
    else if (textField == emailTextField)
    {
        keyboardOffset = 90;
        [self setViewMovedUp:YES  TextField:textField];
    }
    else if (textField == phNoTextField)
    {
        keyboardOffset = 130;
        [self setViewMovedUp:YES  TextField:textField];
    }

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == usernameTextField) {
        [passwordTextField becomeFirstResponder];
    }
    else if (textField == passwordTextField) {
        [textField resignFirstResponder];
        [self readjustloginContainer];
    }
    
    else if (textField == firstNameTextField)
    {
        [secondNameTextField becomeFirstResponder];
    }
    else if (textField == secondNameTextField)
    {
        [emailTextField becomeFirstResponder];
    }
    else if (textField == emailTextField)
    {
        [phNoTextField becomeFirstResponder];
    }
    else
        [textField resignFirstResponder];
    
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    if (textField == firstNameTextField || textField == secondNameTextField || textField == emailTextField || textField == phNoTextField) {
        [self setViewMovedUp:NO TextField:textField];
    }
    
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 50) ? NO : YES;
}

#pragma mark -- Other Methods


- (void)usernameTextFieldDidBegin {
    [self bringTheLoginViewUp];
}

- (void)passwordTextFieldDidBegin {
    [self bringTheLoginViewUp];
}

- (void) bringTheLoginViewUp {
    
    
    [UIView animateWithDuration:0.5 animations:^{
        
        float bottomMargin = 5.0;
        
        CGRect loginContainerFrame = textFieldsBaseView.frame;
        loginContainerFrame.origin.y = self.view.frame.size.height - (loginContainerFrame.size.height + Default_keyboard_height_iPhone + bottomMargin);
        textFieldsBaseView.frame = loginContainerFrame;
    } completion:^(BOOL finished) {
        
    }];
    
    
}

- (void) readjustloginContainer {
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = textFieldsBaseView.frame;
        frame.origin.y = self.view.frame.size.height - frame.size.height - verticalSpace - 3;
        
        textFieldsBaseView.frame = frame;
        //loginContainerView.center = self.view.center;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)setViewMovedUp:(BOOL)movedUp TextField:(UITextField *)TField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    CGRect rect;
    
    rect = self.view.frame;
    
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        NSLog(@"INSIDE MOVEDUP AND SCROLL VIEW Y IS::%f",rect.origin.y);
        
        
        rect.origin.y -= keyboardOffset;
        
        
    }
    else
    {
        rect.origin.y += keyboardOffset;
    }
    self.view.frame = rect;
    [UIView commitAnimations];
}

#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    
    if (request == logingRequest) {
        logingRequest = nil;
        
        NSLog(@"LOGIN:%@",[request responseString]);
        
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        xmlParser.delegate =self;
        [xmlParser parse];
        parser = xmlParser;
        
        
        //^^^ [self removeLoginProgressView];
    }
    else if(request == getUserRequest) {
        
        NSString *responseString = [request responseString];
        NSLog(@"USER DATA:: %@",responseString);
        NSXMLParser *userDetailsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        parserDelegate = [[UserDataXMLParser alloc] init];
        parserDelegate.delegate = self;
        userDetailsParser.delegate = parserDelegate;
        [userDetailsParser parse];
        
    }
    
}

#pragma mark -- NSXMLParser Delegate methods

NSMutableString *parserElementString;



- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"status"]) {
        parserElementString = [[NSMutableString alloc] init];
    }
    else if ([elementName isEqualToString:@"first_status"]) {
        parserElementString = [[NSMutableString alloc] init];
    }
    else if ([elementName isEqualToString:@"first_response"]) {
        parserElementString = [[NSMutableString alloc] init];
    }
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    [parserElementString appendString:string];
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:Status_XML_tag]) {
        
        if ([parserElementString isEqualToString:Status_success_message]) {
            //^^^ [self dismissLoginContainerForUsername:usernameTextField.text password:passwordTextField.text];
            [self getuserDetails];
        }
        else {
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Login Failed" message:@"Please provide valid details" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
        }
        
    }
    
    
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parserLocal {
    parser.delegate = nil;
    parser = nil;
}

- (void)parser:(NSXMLParser *)parserLocal parseErrorOccurred:(NSError *)parseError {
    parser.delegate = nil;
    parser = nil;
}


#pragma mark -- UserDataXMLParser Delegate methods

- (void)parsingUserDetailsFinished:(User *)userDetails {
    
    [self dismissLoginContainerWithUserDetails:userDetails password:password];
    
}

- (void)userDetailXMLparsingFailed {
    
}

- (void)dealloc {
    
    //  NSLog(@"LOGIN DEALLOC");
    
    if (logingRequest) {
        [logingRequest cancel];
        logingRequest = nil;
    }
    
    if (parser) {
        parser.delegate = nil;
        parser = nil;
    }
    
    if (parserDelegate) {
        parserDelegate.delegate = nil;
        parserDelegate = nil;
    }
    
    
    
    
}

#pragma mark facebook start
//-------------------------Sankar--------------------------------
- (void) publishPostWithDelegate:(id) _delegate
{
    
    //store the delegate incase the user needs to login
    //self.delegate = _delegate;
    
    BOOL loggedIn = [[FBRequestWrapper defaultManager] isLoggedIn];
    
    if (!loggedIn) {
        
        NSLog(@"Logged In");
        //isFbLoggedIn=NO;
        [[FBRequestWrapper defaultManager] FBSessionBegin:self];
        
    }
    else {
        
        NSLog(@"Logged Out");
        //isFbLoggedIn=YES;
        [self performSelector:@selector(getFacebookProfile)];
        
    }
    //[self performSelector:@selector(getFacebookProfile)];
}

- (void)getFacebookProfile {
    
    NSLog(@"facebook.accessToken = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"]);
    NSString *urlString = [NSString
                           stringWithFormat:@"https://graph.facebook.com/me?access_token=%@",
                           [[[NSUserDefaults standardUserDefaults] objectForKey:@"access_token"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSLog(@"facebook url %@",url);
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setDidFinishSelector:@selector(getFacebookProfileFinished:)];
    
    [request setDelegate:self];
    [request startAsynchronous];
    
}
- (void)getFacebookProfileFinished:(ASIHTTPRequest *)request
{
    //hidden no
    
    
    // Use when fetching text data
    NSString *responseString = [request responseString];
    // NSLog(@"Got Facebook Profile: %@", [responseString JSONValue]);
    
    NSDictionary *detailsDict = [[NSDictionary alloc]init];
    detailsDict = [responseString JSONValue];
    
    for(NSString *key in [detailsDict allKeys]) {
        NSLog(@"%@",[detailsDict objectForKey:key]);
    }
    

    
    NSLog(@"================================================================");
    NSString *name = [detailsDict objectForKey:@"first_name"];
    NSLog(@"name::%@",name);
    NSString *gender = [detailsDict objectForKey:@"gender"];
    NSLog(@"gender::%@",gender);
    
    NSArray * arr = [detailsDict objectForKey:@"languages"];
    
    NSDictionary *dict = [[NSDictionary alloc]init];
    dict = [arr objectAtIndex:0];
    NSString *lan1 = [dict objectForKey:@"name"];
    dict = [arr objectAtIndex:1];
    NSString *lan2 = [dict objectForKey:@"name"];
    NSLog(@"languages::%@,%@",lan1,lan2);
    
    
    dict = [detailsDict objectForKey:@"location"];
    NSString *location = [dict objectForKey:@"name"];
    NSLog(@"location::%@",location);
    
    arr = [detailsDict objectForKey:@"work"];
    // NSLog(@"arr count::%d",[arr count]);
    dict = [arr objectAtIndex:0];
    
    //    for(NSString *key in [dict allKeys]) {
    //        NSLog(@"%@",[dict objectForKey:key]);
    //    }
    NSArray *arr1 = [[NSArray alloc]init];
    arr1 = [dict allValues];
    //NSLog(@"arr1 count::%d",[arr1 count]);
    
    
    [dict setValue:[arr1 objectAtIndex:0] forKey:@"employer"];
    [dict setValue:[arr1 objectAtIndex:2] forKey:@"position"];
    
    NSDictionary *dict1 = [dict objectForKey:@"employer"];
    NSString *emplyee = [dict1 objectForKey:@"name"];
    NSLog(@"Company::%@",emplyee);
    dict1 = [dict objectForKey:@"position"];
    NSString *position = [dict1 objectForKey:@"name"];
    NSLog(@"Designation::%@",position);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"access_token"];
    [defaults removeObjectForKey:@"facebook_user"];
    [defaults removeObjectForKey:@"facebook_userid"];
    [defaults removeObjectForKey:@"exp_date"];
    
}

- (void)fbDidLogin {
    
    
    
    //after the user is logged in try to publish the post
    
    //https://graph.facebook.com/100000838671085/picture/
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"facebook_user"])
    {
        [self performSelector:@selector(getFacebookProfile)];
        
        
    }
    else
    {
        
    }
    
}
//@"access_token"
//@"exp_date"

-(void)fbDidExtendToken:(NSString *)accessTokens expiresAt:(NSDate *)expiresAt {
    NSLog(@"token extended");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:accessTokens forKey:@"access_token"];
    [defaults setObject:expiresAt forKey:@"exp_date"];
    
    NSLog(@"access_token  -----> %@",[defaults objectForKey:@"access_token"]);
    NSLog(@"exp_date  -----> %@",[defaults objectForKey:@"exp_date"]);
    
    [defaults synchronize];
}




- (void)request:(FBRequest *)request didLoad:(id)result
{
    NSLog(@"ViewController's request:didLoad: method executing %@",result);
    
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"[appDelegate request:(FBRequest *) didFailWithError:]");
}

- (void)fbDidNotLogin:(BOOL)cancelled
{
    
    NSLog(@"fbDidNotLogin/failedToPublishPost");
    //    [spinner stopAnimating];
    //
    //    if (fbPost) {
    //        fbPost.delegate = nil;
    //        fbPost = nil;
    //    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)fbDidLogout
{
    [[FBRequestWrapper defaultManager] setIsLoggedIn:NO];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // [facebook invalidateSession];
    //[defaults removeObjectForKey:@"FBAccessTokenKey"];
    //[defaults removeObjectForKey:@"FBExpirationDateKey"];
    [defaults removeObjectForKey:@"access_token"];
    [defaults removeObjectForKey:@"facebook_user"];
    [defaults removeObjectForKey:@"facebook_userid"];
    [defaults removeObjectForKey:@"exp_date"];
    
    NSLog(@"access_token/fbdidlogout is %@",[defaults objectForKey:@"access_token"]);
    NSLog(@"facebook_user/fbdidlogout is %@",[defaults objectForKey:@"facebook_user"]);
    NSLog(@"facebook_userid/fbdidlogout is %@",[defaults objectForKey:@"facebook_userid"]);
    NSLog(@"exp_date/fbdidlogout is %@",[defaults objectForKey:@"exp_date"]);
    
    [defaults synchronize];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
