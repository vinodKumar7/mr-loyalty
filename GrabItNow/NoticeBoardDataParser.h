//
//  NoticeBoardDataParser.h
//  GrabItNow
//
//  Created by MyRewards on 3/25/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NoticeBoard.h"


@protocol NoticeBoardXMLParserDelegate;

@interface NoticeBoardDataParser : NSObject<NSXMLParserDelegate>{
    __unsafe_unretained id <NoticeBoardXMLParserDelegate> delegate;
}
@property (unsafe_unretained) id <NoticeBoardXMLParserDelegate> delegate;
@end


@protocol NoticeBoardXMLParserDelegate <NSObject>

- (void) parsingNoticeBoardDataFinished:(NSArray *) noticesList;
- (void) parsingNoticeBoardDataXMLFailed;

@end