//
//  KeyboardViewController.h
//  PDKTStickySectionHeadersCollectionViewLayoutDemo
//
//  Created by vairat on 17/03/15.
//  Copyright (c) 2015 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface KeyboardViewController : UIViewController <UITextFieldDelegate,UITextInputDelegate>

{
    
}

@property (nonatomic, strong) UITextField *currentTextField;
@property (strong, nonatomic) IBOutlet UIView *accessoryView;
@property (strong, nonatomic) IBOutlet UITextField *presentTextField;
@property (strong, nonatomic) IBOutlet UILabel *merchantMustLabel;
@property (strong, nonatomic) NSString *deviceID;
@property (strong, nonatomic) NSString *multipleRedeemCount;



@property (strong, nonatomic) IBOutlet UIImageView *product_imageView;
@property (strong, nonatomic) IBOutlet UILabel *lblCoffeeFree;
@property (strong, nonatomic) IBOutlet UILabel *lblProductName;

@property (strong,nonatomic) NSString *product_ID;
@property (strong, nonatomic) NSString *product_Name;
@property (strong, nonatomic) UIImage *product_Image;
@property (strong,nonatomic) NSString *product_Offer;
@property (strong,nonatomic) NSString *redeemPassword;


@property (assign, nonatomic) BOOL isLastStamp;
@property (nonatomic, assign) BOOL isFromFreebies;

- (IBAction)backBtnPressed:(id)sender;

@end
