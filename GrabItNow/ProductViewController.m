//
//  ProductViewController.m
//  GrabItNow
//
//  Created by MyRewards on 12/25/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "ProductViewController.h"
#import "ASIFormDataRequest.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "Merchant.h"
#import "ProductCell.h"
#import "MerchantAddressCell.h"
#import "HomeViewController.h"
#import "NSString_stripHtml.h"

#define ProductCellHeight 64.0;



@interface ProductViewController ()
{
    UIView *offerView;
    UIView *contactsView;
    UIView *addressView;
    ASIFormDataRequest *imageRequest;
    ASIFormDataRequest * merchantFetchRequest;
    ASIFormDataRequest *redeemRequest;
    Product *thisProduct;
    Merchant *merchant;
    MerchantListParser *merchantListParser;
    AppDelegate *appDelegate;
    BOOL addressDetailsLoaded;
    UIView *loadingView;
    
    int contactLabelOrigin_Y;
    NSString *callNumber;
}

@property (nonatomic, strong) UIView *offerView;
@property (nonatomic, strong) UIView *contactsView;
@property (nonatomic, strong) UIView *addressView;
@property (nonatomic, strong) Product *thisProduct;
@end

@implementation ProductViewController

@synthesize Location;
@synthesize userLocation;
@synthesize locationManager;
@synthesize couponMerchantImage;
@synthesize couponImageView;
@synthesize TandCButton;
@synthesize TandCView;
@synthesize TandCLabel;
@synthesize couponTandCTextView;
@synthesize TandCBackButton;
@synthesize couponFlipView;
@synthesize couponBgImageView;
@synthesize couponCardBackBg_ImageView;
@synthesize couponFrontView;
@synthesize couponBackView;
@synthesize redeemActivityIndicator;
@synthesize containerView;
@synthesize fromNearbyMe;

@synthesize offerView;
@synthesize contactsView;
@synthesize addressView;

@synthesize thisProduct;
@synthesize couponCardBg_ImageView;

@synthesize scroller;
@synthesize imageActivityIndicator;

@synthesize imgContainerView;
@synthesize imageView;
@synthesize favButton;
@synthesize redeemButton;
@synthesize couponButton;
@synthesize productNameLabel;
@synthesize productOfferLabel;
@synthesize frontRedeemButton;

@synthesize mainContentHolder;
@synthesize offerButton;
@synthesize contactButton;
@synthesize addressButton;
@synthesize noImageLabel;
@synthesize couponView;
@synthesize discountLabel;
@synthesize memNameLabel,memNumberLabel,clientLabel;
@synthesize delegate;
@synthesize merchantTableView,mailComposeView;
@synthesize myCouponImage;

- (void)dealloc {
    
    if (imageRequest) {
        imageRequest.delegate = nil;
       // [imageRequest cancel];
        imageRequest = nil;
    } 
   
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andProduct:(Product *) pro
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        thisProduct = pro;
        fromNearbyMe = NO;
        addressDetailsLoaded = NO;
        self.myCouponImage = nil;
        //NSLog(@"\n #################### Product ID inside init method of ProductViewController = %@",thisProduct.productId);
        //NSLog(@"\n #################### Product offer inside init method of ProductViewController = %@",thisProduct.productOffer);
    }
    return self;
}

- (void) updateFavoriteStatus {
    if ([appDelegate productExistsInFavorites:thisProduct]) {
        //[favButton setTitle:@"Remove favorite" forState:UIControlStateNormal];
        [favButton setBackgroundImage:[UIImage imageNamed:@"heart_full.png"] forState:UIControlStateNormal];
    }
    else {
        //[favButton setTitle:@"Add to favorite" forState:UIControlStateNormal];
        [favButton setBackgroundImage:[UIImage imageNamed:@"heart_empty.png"] forState:UIControlStateNormal];
    }
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    phnoCount=0;
  
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self updateFavoriteStatus];
    
    if (thisProduct.productImage) {
        [self.imageView setImage:thisProduct.productImage];
    }
    
    offerButton.layer.borderColor = contactButton.layer.borderColor = addressButton.layer.borderColor = [UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0].CGColor;
    offerButton.layer.borderWidth = contactButton.layer.borderWidth = addressButton.layer.borderWidth = 2.0;
    couponButton.layer.borderColor = [UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0].CGColor;
    couponButton.layer.borderWidth = 2.0;
    
    imgContainerView.layer.cornerRadius = 5.0;
    imgContainerView.layer.borderColor = [UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0].CGColor;
    imgContainerView.layer.borderWidth = 2.0;
    
    couponMerchantImage.layer.cornerRadius = 5.0;
    couponMerchantImage.layer.borderColor = [UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0].CGColor;
    couponMerchantImage.layer.borderWidth = 2.0;
    
    imageView.layer.borderWidth = 2.0;
    imageView.layer.borderColor = [UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0].CGColor;
    imageView.layer.cornerRadius = 4.0;
    
    
    // Intially show favorite screen
    [self offerButtonPressed:offerButton];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if(fromNearbyMe) {
        
        [appDelegate.homeViewController hideBackButon:NO];
        [appDelegate.homeViewController setHeaderTitle:@"Product"];
        
        // Create cell view
        ProductCell *cell = nil;
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"ProductCell" owner:nil options:nil];
        
        for (UIView *cellview in views) {
            if([cellview isKindOfClass:[UITableViewCell class]]) {
                cell = (ProductCell*)cellview;
            }
        }
        
        // apply shadow
        cell.headerContainerView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
        cell.headerContainerView.layer.shadowOpacity = 1.0;
        cell.headerContainerView.layer.shadowOffset = CGSizeMake(0.0, 5.0);
        cell.headerContainerView.layer.shadowRadius = 5.0;
        cell.headerContainerView.backgroundColor = [UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0];
        cell.productNameLabel.textColor=[UIColor colorWithRed:212/255.0 green:237/255.0 blue:255/255.0 alpha:1.0];
        cell.productNameLabel.text = thisProduct.productName;
        cell.productOfferLabel.text = thisProduct.productOffer;
        
        [self.view addSubview:cell];
        
  //============== Adding button to cell to enable back action
        
        UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
        myButton.frame = CGRectMake(0,0, 320.0, 64.0);
        [myButton addTarget:self
                   action:@selector(cellpressed:)
         forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:myButton];
        [self.view bringSubviewToFront:myButton];
  
  //==================
        // Update cell height
        CGRect cellFrame = cell.frame;
        cellFrame.origin.y = 0.0;
        cell.frame = cellFrame;
        
        // Update view size
        CGRect viewFrame = self.containerView.frame;
        viewFrame.origin.y = cell.frame.size.height;
        viewFrame.size.height = viewFrame.size.height - cell.frame.size.height;
        self.containerView.frame = viewFrame;
        
        if (!couponView.hidden) {
            [self.view bringSubviewToFront:couponView];
        }
    }
    
}


-(void) cellpressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    scroller.frame = self.containerView.bounds;
}

/*
-(void)getCurrentLocation
{
    //userlocation tracking
    locationManager = [[CLLocationManager alloc] init];
    //locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    
}
 tringByReplacingOccurrencesOfString:@"&"
 withString:@"and"
*/


//- (void)viewWillLayoutSubviews {
//
//
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) bringForwardTab:(UIButton *) tabBtn {
    
    if (tabBtn == offerButton) {
        
        //[scroller insertSubview:addressButton belowSubview:offerButton];
        //[scroller insertSubview:contactButton belowSubview:offerButton];
        
        offerButton.backgroundColor = [UIColor purpleColor];
        contactButton.backgroundColor = [UIColor whiteColor];
        addressButton.backgroundColor = [UIColor whiteColor];
        
        [offerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [contactButton setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
        [addressButton setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
        
        [offerButton setBackgroundImage:[UIImage imageNamed:@"sale1.png"] forState:UIControlStateNormal];
        [contactButton setBackgroundImage:[UIImage imageNamed:@"phone.png"] forState:UIControlStateNormal];
        [addressButton setBackgroundImage:[UIImage imageNamed:@"drop.png"] forState:UIControlStateNormal];
        
    }
    else if (tabBtn == contactButton) {
        //[scroller insertSubview:offerButton belowSubview:contactButton];
        //[scroller insertSubview:addressButton belowSubview:contactButton];
        
        offerButton.backgroundColor = [UIColor whiteColor];
        contactButton.backgroundColor = [UIColor purpleColor];
        addressButton.backgroundColor = [UIColor whiteColor];
        
        [offerButton setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
        [contactButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [addressButton setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
        
        [offerButton setBackgroundImage:[UIImage imageNamed:@"sale.png"] forState:UIControlStateNormal];
        [contactButton setBackgroundImage:[UIImage imageNamed:@"phone1.png"] forState:UIControlStateNormal];
        [addressButton setBackgroundImage:[UIImage imageNamed:@"drop.png"] forState:UIControlStateNormal];
    }
    else if (tabBtn == addressButton) {
        //[scroller insertSubview:offerButton belowSubview:addressButton];
        //[scroller insertSubview:contactButton belowSubview:addressButton];
        
        offerButton.backgroundColor = [UIColor whiteColor];
        contactButton.backgroundColor = [UIColor whiteColor];
        addressButton.backgroundColor = [UIColor purpleColor];
        
        [offerButton setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
        [contactButton setTitleColor:[UIColor purpleColor] forState:UIControlStateNormal];
        [addressButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [offerButton setBackgroundImage:[UIImage imageNamed:@"sale.png"] forState:UIControlStateNormal];
        [contactButton setBackgroundImage:[UIImage imageNamed:@"phone.png"] forState:UIControlStateNormal];
        [addressButton setBackgroundImage:[UIImage imageNamed:@"drop1.png"] forState:UIControlStateNormal];
    }
    
}

- (void) cleanMainController {
    
    for (UIView *subView in mainContentHolder.subviews) {
        [subView removeFromSuperview];
    }
    
}

- (void) updateScrollerContentSize {
    
    scroller.frame = self.containerView.bounds;
    
    float contentHeight = mainContentHolder.frame.origin.y + mainContentHolder.frame.size.height +10.0;
    
    scroller.contentSize = CGSizeMake(scroller.contentSize.width, contentHeight);
    
    NSLog(@"** scroller contentSize: %@",NSStringFromCGSize(scroller.contentSize));
    NSLog(@"** scroller frame: %@",NSStringFromCGRect(scroller.frame));
    
    scroller.scrollEnabled = YES;
    
}

- (IBAction) favoritesButtonPressed:(id) sender {
    
    if (![appDelegate productExistsInFavorites:thisProduct]) {
        //[appDelegate removeProductFromfavorites:thisProduct];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Add to favorites?" message:[NSString stringWithFormat:@"Do you want to add %@ to favorites",thisProduct.productName] delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        alert.tag = 21;
        [alert show];
    }
    else {
        //[appDelegate addProductToFavorites:thisProduct];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Remove favorite?" message:[NSString stringWithFormat:@"Do you want to remove %@ from favorites",thisProduct.productName] delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        alert.tag = 22;
        [alert show];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
        if (alertView.cancelButtonIndex == buttonIndex) {
            if (alertView.tag == 21) {
                // Add to fav case
                BOOL success = [appDelegate addProductToFavorites:thisProduct];
                
                [self updateFavoriteStatus];
                
                
                if (success && delegate && [delegate respondsToSelector:@selector(productAddedToFavorites:)]) {
                    [delegate productAddedToFavorites:thisProduct];
                }
                
            }
            else if (alertView.tag == 22) {
                // Remove from fav case
                BOOL success = [appDelegate removeProductFromfavorites:thisProduct];
                
                [self updateFavoriteStatus];
                
                if (success && delegate && [delegate respondsToSelector:@selector(productremovedFromFavorites:)]) {
                    [delegate productremovedFromFavorites:thisProduct];
                }
                
            }
            
            if(alertView.tag == 111)
            {
                NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@",thisProduct.phone];
                NSString *newString = [phoneURLString stringByReplacingOccurrencesOfString:@" " withString:@""];
                NSLog(@"phone no. is %@",newString);
                NSURL *phoneURL = [NSURL URLWithString:newString];
                [[UIApplication sharedApplication] openURL:phoneURL];
            }

        }
        else
        {
                       if(alertView.tag == 222)
            {
                NSLog(@"Redeem alert view and button index::%d",buttonIndex);
                if(buttonIndex == 1)
                {
                    
                    NSString *lattitude = @"0.00";   // [NSString stringWithFormat:@"%f",self.userLocation.latitude];
                    NSString *longitude = @"0.00";// [NSString stringWithFormat:@"%f",self.userLocation.longitude];
                    // NSLog(@"%@",lattitude);
                    // NSLog(@"%@",longitude);
                    
                    NSString *urlString = [NSString stringWithFormat:@"%@redeemed.php",URL_Prefix];
                    NSURL *url = [NSURL URLWithString:urlString];
                    NSLog(@"url is:==> %@",url);
                    
                    
                    redeemRequest = [[ASIFormDataRequest alloc] initWithURL:url];
                    [redeemRequest setPostValue:appDelegate.sessionUser.userId forKey:@"user_id"];
                    [redeemRequest setPostValue:thisProduct.productId forKey:@"pid"];
                    [redeemRequest setPostValue:appDelegate.sessionUser.client_id forKey:@"cid"];
                    [redeemRequest setPostValue:lattitude forKey:@"lat"];
                    [redeemRequest setPostValue:longitude forKey:@"lon"];
                    
                    [redeemRequest setDelegate:self];
                    [redeemRequest startAsynchronous];
                    [self.redeemActivityIndicator startAnimating];
                    
                    
                    
                }
            }
            if(alertView.tag == 333)
            {
                NSLog(@"ThankYou alert view and button index::%d",buttonIndex);
                if(buttonIndex == 0)
                {
                    
                    [self handleCouponTap:nil];
                    
                }
            }
        }//else
    }

- (IBAction) redeemButtonPressed:(id) sender {
    
    //[delegate showMailComposer:nil withBody:nil];
    //[appDelegate.homeViewController showMailComposer:nil withBody:nil];
    
    
    
    UIAlertView *redemAlert = [[UIAlertView alloc]initWithTitle:@"The redeem button is for merchant use only." message:@"A merchant  will press this to record your redemption. Some offers/vouchers do not permit multiple use, so don't waste a voucher.\n\n" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    
    
    UILabel *txtField = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 160.0, 260.0, 45.0)];
    [txtField setFont:[UIFont fontWithName:@"Helvetica-Bold" size:(15.0)]];
    txtField.textAlignment = UITextAlignmentCenter;
    txtField.numberOfLines = 2;
    txtField.textColor = [UIColor whiteColor];
    txtField.text = @"If you are not a merchant, Press No now to go back.";
    txtField.backgroundColor = [UIColor clearColor];
    [redemAlert addSubview:txtField];
    redemAlert.tag = 222;
    [redemAlert show];
    
    
    
}


- (IBAction) couponButtonPressed:(id) sender
{
    if ([thisProduct.mobilecoupon isEqualToString:@"0"])
    {
        UIAlertView *CouponAlert=[[UIAlertView alloc]initWithTitle:@"This is NOT a coupon offer" message:@"  Refer to offer terms for redemption instructions" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
        CouponAlert.tag=123;
        [CouponAlert show];
        
    }
    else

{
    
    if (!fromNearbyMe) {
        // Alternate procedure
        NSLog(@"couponButtonPressed");
        [delegate showProductCouponForProduct:thisProduct couponImage:self.myCouponImage];
        return;
    }
    
    
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(couponGestureSelector:)];
    recognizer.delegate = self;
    [couponView addGestureRecognizer:recognizer];
    
    couponView.frame = self.view.bounds;/*containerView.bounds*/;
    
    
    
    
    
    
    couponView.hidden = NO;
    
    couponView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.9];
   // couponView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:couponView];
    
    discountLabel.text = [NSString stringWithFormat:@"%@",thisProduct.productOffer];
    productNameLabel.text = thisProduct.productName;
    //productOfferLabel.text = thisProduct.productOffer; //same as discount
    couponMerchantImage.image = self.myCouponImage; //thisProduct.productImage;
    
    memNameLabel.text =[NSString stringWithFormat:@"Name: %@ %@",appDelegate.sessionUser.first_name, appDelegate.sessionUser.last_name];
    clientLabel.text = [NSString stringWithFormat:@"Client: %@", appDelegate.sessionUser.client_name];
    memNumberLabel.text = [NSString stringWithFormat:@"Membership: %@", appDelegate.sessionUser.username];
   
        
    
    
    
    // Finalizing statements
    couponCardBg_ImageView.frame = couponFlipView.bounds;
    couponCardBackBg_ImageView.frame =  couponFlipView.bounds;
    couponBackView.frame = couponFlipView.bounds;
    couponFrontView.frame = couponFlipView.bounds;
    
    //======== configuring couponcard buttons and labels
           
    
    //buttons
    CGRect TandCButtonFrame = TandCButton.frame;
    TandCButtonFrame.origin.x = couponImageView.frame.origin.x + couponImageView.frame.size.width - TandCButtonFrame.size.width-22;
    if([appDelegate isIphone5])
        TandCButtonFrame.origin.y = couponFrontView.frame.size.height-70;
    else
        TandCButtonFrame.origin.y = couponFrontView.frame.size.height-60;
    TandCButton.frame = TandCButtonFrame;
    
    CGRect fRedeemButtonFrame = frontRedeemButton.frame;
    fRedeemButtonFrame.origin.x = couponImageView.frame.origin.x + couponImageView.frame.size.width - fRedeemButtonFrame.size.width-194;
    
    if([appDelegate isIphone5])
        fRedeemButtonFrame.origin.y = couponFrontView.frame.size.height-70;
    else
        fRedeemButtonFrame.origin.y = couponFrontView.frame.size.height-60;
    frontRedeemButton.frame = fRedeemButtonFrame;
    
    
    
    CGRect TandCBackButtonFrame = TandCBackButton.frame;
    TandCBackButtonFrame.origin.x = 40.0;
    TandCBackButtonFrame.origin.y = couponBackView.frame.size.height-65;
    TandCBackButton.frame = TandCBackButtonFrame;
    
    CGRect redeemButtonFrame = redeemButton.frame;
    redeemButtonFrame.origin.x = couponImageView.frame.origin.x + couponImageView.frame.size.width - redeemButtonFrame.size.width - 23.0;
    redeemButtonFrame.origin.y = couponBackView.frame.size.height-65;
    redeemButton.frame = redeemButtonFrame;
    
    
    
    CGRect discountLabelFrame = discountLabel.frame;
    discountLabelFrame.origin.y = self.productNameLabel.frame.origin.y + self.productNameLabel.frame.size.height+20;
    discountLabel.frame = discountLabelFrame;
    
    CGRect nameLabelFrame = memNameLabel.frame;
    if ([appDelegate isIphone5])
        nameLabelFrame.origin.y = discountLabel.frame.origin.y+discountLabel.frame.size.height ;
    else 
        nameLabelFrame.origin.y = 220;
    memNameLabel.frame = nameLabelFrame;
    
    CGRect memnumLabelFrame = memNumberLabel.frame;
    memnumLabelFrame.origin.y = self.memNameLabel.frame.origin.y + self.memNameLabel.frame.size.height;
    memNumberLabel.frame = memnumLabelFrame;
    
    CGRect clientLabelFrame = clientLabel.frame;
    clientLabelFrame.origin.y = self.memNumberLabel.frame.origin.y + self.memNumberLabel.frame.size.height;
    clientLabel.frame = clientLabelFrame;

    CGRect couponBgFrame = couponBgImageView.frame;
    if([appDelegate isIphone5])
        couponBgFrame.size.height = 170;
    else  couponBgFrame.size.height = 120;
    couponBgImageView.frame = couponBgFrame;
    
    
    
    
    //===========
    [couponFlipView addSubview:couponBackView];
    [couponFlipView addSubview:couponFrontView];
    couponBackView.alpha = 0.0;
    couponFrontView.alpha = 1.0;
    
    
    
    
    
    }
}

- (IBAction) termsAndCondButtonPressed:(id)sender {
    
    NSLog(@"termsAndCondButtonPressed");
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:couponFlipView cache:NO];
    
    NSLog(@"%@",thisProduct.productTermsAndConditions);
   // NSLog(@"T&C: %@",pro.productTermsAndConditions);
    NSString *htmlString = thisProduct.productTermsAndConditions;
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&"
                                                       withString:@"and"];

    couponTandCTextView.text = [htmlString stripHtml];
    
    //couponMerchantImage.image = thisProduct.productImage;
    
    NSLog(@"::thisProduct.termsAndConditions=%@",thisProduct.productTermsAndConditions);
    couponFrontView.alpha = 0.0;
    couponBackView.alpha = 1.0;
    [UIView commitAnimations];
    
}

- (IBAction) termsAndCondBackButtonPressed:(id)sender {
    NSLog(@"termsAndCondBackButtonPressed");
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:couponFlipView cache:NO];
    
    couponFrontView.alpha = 1.0;
    couponBackView.alpha = 0.0;
    [UIView commitAnimations];
}

- (void) couponGestureSelector:(UIGestureRecognizer *) gesRec {
    
    CGPoint touchPoint = [gesRec locationOfTouch:0 inView:couponView];
    
    if (couponFrontView.alpha == 0.0) {
        // i.e Front view is not visible, but backView is
        if( CGRectContainsPoint(TandCBackButton.frame, touchPoint) ) {
            [self termsAndCondBackButtonPressed:TandCBackButton];
        }
        else if( CGRectContainsPoint(redeemButton.frame, touchPoint) ) {
            [self redeemButtonPressed:redeemButton];
        }
        else {
            [self handleCouponTap:nil];
        }
    }
    else
    {
        // i.e Back view is not visible, but frontView is
        if( CGRectContainsPoint(TandCButton.frame, touchPoint) )
        {
            [self termsAndCondButtonPressed:TandCButton];
        }
        else if( CGRectContainsPoint(frontRedeemButton.frame, touchPoint) )
        {
            [self redeemButtonPressed:redeemButton];
        }
        else
        {
            [self handleCouponTap:nil];
        }
    }
}


-(IBAction)handleCouponTap:(id)sender
{
    NSLog(@"handleCouponTap:");
    couponView.hidden = YES;
    [self.containerView sendSubviewToBack:couponView];
}

- (void) addOfferViewAsSubview {
    
    CGRect mainFrame = mainContentHolder.frame;
    mainFrame.size = offerView.frame.size;
    mainContentHolder.frame = mainFrame;
    
    [mainContentHolder addSubview:offerView];
    
    [self updateScrollerContentSize];
    
}

- (void) addContactsViewAsSubview {
    
    contactsView.frame = mainContentHolder.bounds;
    
    [mainContentHolder addSubview:contactsView];
    
    [self updateScrollerContentSize];
    
}

- (void) addAddressViewAsSubview {
    
    addressView.frame = mainContentHolder.bounds;
    
    [mainContentHolder addSubview:addressView];
    
    [self updateScrollerContentSize];
    
}


- (void) updateOfferView {
    
    if (!offerView) {
        return;
    }
    
    // Check if webview already exists
    
    UIWebView *offerWebView;
  //  offerWebView.scalesPageToFit = YES;
    offerWebView.delegate=self;
    if ((offerView.subviews.count != 0) ) {
        offerWebView = (UIWebView *)[offerView viewWithTag:2012];
        
    }
    else {
        offerWebView = [[UIWebView alloc] initWithFrame:offerView.bounds];
        
        NSLog(@"ELSE WEBVIEW");
            }
    
    NSString *webViewtext;
    if ([thisProduct.productDesciption length] <= 0 && [thisProduct.productText length] <= 0)
        webViewtext = @"";
     else if ([thisProduct.productDesciption length] > 0&& [thisProduct.productText length] > 0)
    {
    webViewtext = [NSString stringWithFormat:@"%@%@",thisProduct.productDesciption,thisProduct.productText];
    }else
    {
        if ([thisProduct.productDesciption length] > 0) {
             webViewtext = [NSString stringWithFormat:@"%@",thisProduct.productDesciption];
        }
    
        if ([thisProduct.productText length] > 0) {
            webViewtext = [NSString stringWithFormat:@"%@",thisProduct.productText];
        }

    }
    
    
    NSLog(@" webtext is..... %@ ",webViewtext);
    NSLog(@" product offers is..... %@ ",thisProduct.productOffer);
    NSString *myDescriptionHTML = @"";
    NSLog(@"QUANTITY:%@",thisProduct.quantity);
    
    if ([thisProduct.quantity isEqualToString:@"0"]||
        [thisProduct.quantity length]<=0)
    {
        myDescriptionHTML =  [NSString stringWithFormat:@"<html> \n"
                              "<head> \n"
                              "<style type=\"text/css\"> \n"
                              "body {font-family: \"%@\"; font-size: %@;line-height:1.5%;}\n"
                              "H4{ color: rgb(198,38,21) }\n"
                              "</style> \n"
                              "<style type='text/css'>body { max-width: 300%; width: auto; height: auto; }</style>"
                              "<style type='text/css'>img { max-width: 300%; width: auto; height: auto; }</style>"
                              "</head> \n"
                              "<body><H4>%@</H4>%@</body> \n"
                              "</html>", @"Helvetica", [NSNumber numberWithInt:13],thisProduct.productOffer, webViewtext];
    
    
        NSString *shopNowURL = [NSString stringWithFormat:@"http://www.perksatwork.com.au/?user=%@",appDelegate.sessionUser.username];
        
        NSLog(@"shop now url is %@ ",shopNowURL);
    
    }
    else{
        NSString *shopNowURL = [NSString stringWithFormat:@"http://www.perksatwork.com.au/?user=%@&pageid=%@",appDelegate.sessionUser.username,thisProduct.quantity];
        
        NSLog(@"shop now url is %@ ",shopNowURL);
        
        myDescriptionHTML =  [NSString stringWithFormat:@"<html> \n"
                              "<head> \n"
                              "<style type=\"text/css\"> \n"
                              "body {font-family: \"%@\"; font-size: %@;line-height:1.5%;}\n"
                              "H4{ color: rgb(198,38,21) }\n"
                              "</style> \n"
                              "<style type='text/css'>body { max-width: 300%; width: auto; height: auto; }</style>"
                              "<style type='text/css'>img { max-width: 300%; width: auto; height: auto; }</style>"
                              "</head> \n"
                              "<body><H4>%@</H4><a href=%@  style=color:rgb(198,38,21)>Shop Now</a>%@</body> \n"
                              "</html>", @"Helvetica", [NSNumber numberWithInt:13],thisProduct.productOffer,shopNowURL,webViewtext];
        
        
    }
    
    
    NSString *stripped = [myDescriptionHTML stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    NSLog(@"html is %@",stripped);
     NSLog(@"html is %@",myDescriptionHTML);
    [offerWebView loadHTMLString:stripped baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    offerWebView.tag = 2012;
 //   offerWebView.scalesPageToFit = YES;
 //   offerWebView.multipleTouchEnabled= YES;
    offerWebView.delegate = self;
    [offerView addSubview:offerWebView];
    
   // NSLog(@"phone number:;%@",thisProduct.phone);
    // Add DETAILS here
    
    /*
     
     float commonYGap = 15.0;
     float xMargin = 10.0;
     float commonWidth = offerView.frame.size.width - 2*xMargin;
     
    */
    /*
     
    UILabel *productDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(xMargin, commonYGap, commonWidth, 30.0)];
    productDescLabel.numberOfLines = 0;
    productDescLabel.backgroundColor = [UIColor clearColor];
    productDescLabel.textColor = [UIColor darkGrayColor];
    
    UIFont *commonFont = [UIFont systemFontOfSize:17.0];
    
    CGSize proDescConstraintSize = [thisProduct.productDesciption sizeWithFont:commonFont constrainedToSize:CGSizeMake(commonWidth, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    
    productDescLabel.text = thisProduct.productDesciption;
    
    CGRect finalProductDescFrame = productDescLabel.frame;
    finalProductDescFrame.size = proDescConstraintSize;
    productDescLabel.frame = finalProductDescFrame;
    
    [offerView addSubview:productDescLabel];
    */
    // Add Product detail description here
    /*
    float proDetDescLblY = productDescLabel.frame.size.height + productDescLabel.frame.origin.y + commonYGap;
    
    UILabel *productDetailedDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(xMargin, proDetDescLblY, commonWidth, 30.0)];
    productDetailedDescLabel.backgroundColor = [UIColor clearColor];
    productDetailedDescLabel.textColor = [UIColor darkGrayColor];
    
    CGSize proDetailedDescConstraintSize = [thisProduct.productDetailedDesciption sizeWithFont:commonFont constrainedToSize:CGSizeMake(commonWidth, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    
    productDetailedDescLabel.text = thisProduct.productDetailedDesciption;
    
    CGRect finalProductDetailedDescFrame = productDetailedDescLabel.frame;
    finalProductDetailedDescFrame.size = proDetailedDescConstraintSize;
    productDetailedDescLabel.frame = finalProductDetailedDescFrame;
    
    [offerView addSubview:productDetailedDescLabel];
    */
    // Add terms and conditions here
    /*
    float proTACLblY = productDetailedDescLabel.frame.size.height + productDetailedDescLabel.frame.origin.y + commonYGap;
    
    UILabel *TACDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(xMargin, proTACLblY, commonWidth, 30.0)];
    TACDescLabel.backgroundColor = [UIColor clearColor];
    TACDescLabel.textColor = [UIColor darkGrayColor];
    
    CGSize proTACConstraintSize = [thisProduct.productTermsAndConditions sizeWithFont:commonFont constrainedToSize:CGSizeMake(commonWidth, 9999) lineBreakMode:NSLineBreakByWordWrapping];
    
    TACDescLabel.text = thisProduct.productTermsAndConditions;
    
    CGRect finalTACFrame = TACDescLabel.frame;
    finalTACFrame.size = proTACConstraintSize;
    TACDescLabel.frame = finalTACFrame;
    
    [offerView addSubview:TACDescLabel];
     */
}




//=======
-(BOOL)doesString:(NSString *)string containCharacter:(char)character
{
    if ([string rangeOfString:[NSString stringWithFormat:@"%c",character]].location != NSNotFound)
    {
        return YES;
    }
    return NO;
}


//=======


- (void) updateContactsView {
    NSLog(@"\n merchant name = %@", thisProduct.contactMerchantName);
    if (!contactsView) {
        return;
    }
    
    NSLog(@"phno::%@",thisProduct.phone);
    NSLog(@"website link::%@",thisProduct.websiteLink);
    
    NSString* phnoString = [thisProduct.phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    phArray = [[NSArray alloc]init];
    phMutableArray = [[NSMutableArray alloc]init];
    
    //    phArray = [phnoString componentsSeparatedByString:@"/,"];
    phArray = [phnoString componentsSeparatedByCharactersInSet:
               [NSCharacterSet characterSetWithCharactersInString:@"/,"]
               ];
    NSLog(@"strings %@",phArray);
    
    if([phArray count] !=  0)
    {
        contactLabelOrigin_Y = 5;
        for (int i = 0; i < [phArray count]; i++) {
            
            UILabel *contactlabel = [[UILabel alloc]initWithFrame:CGRectMake(60, contactLabelOrigin_Y, 150, 30)];
            [contactsView addSubview:contactlabel];
            
            if ([[phArray objectAtIndex:i] length] <= 4) {
                
                NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:[[phArray objectAtIndex:i-1] length] - [[phArray objectAtIndex:i] length]];
                
                for (int j=0; j < [[phArray objectAtIndex:i-1] length] - [[phArray objectAtIndex:i] length]; j++) {
                    NSString *ichar  = [NSString stringWithFormat:@"%c", [[phArray objectAtIndex:i-1] characterAtIndex:j]];
                    [characters addObject:ichar];
                }
                
                NSString * newString = [[characters valueForKey:@"description"] componentsJoinedByString:@""];
                contactlabel.text = [newString stringByAppendingString:[phArray objectAtIndex:i]];
                
                [phMutableArray addObject:contactlabel.text];
            }
            else
            {
                contactlabel.text = [phArray objectAtIndex:i];
                [phMutableArray addObject:[phArray objectAtIndex:i]];
            }
            
            UIButton *contactlabel_Button = [[UIButton alloc]initWithFrame:CGRectMake(30, contactLabelOrigin_Y+3, 25, 25)];
            contactlabel_Button.tag = i;
            [contactlabel_Button addTarget:self action:@selector(callButtonpressed:)forControlEvents:UIControlEventTouchUpInside];
            [contactlabel_Button setImage:[UIImage imageNamed:@"phones.png"] forState:UIControlStateNormal];
            
            [contactsView addSubview:contactlabel_Button];
            
            contactLabelOrigin_Y = contactLabelOrigin_Y + 30;
        }
        
    }
    else
    {
        UILabel *contactsLabel  = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 150, 50)];
        
        contactsLabel.text = @"No contact Number Available";
        
        
        contactsLabel.numberOfLines = 0;
        contactsLabel.font = [UIFont systemFontOfSize:15.0];
        [contactsView addSubview:contactsLabel];
        
    }
    
    
    // website button and label
    if([thisProduct.websiteLink length] > 0)
    {
        UILabel *websiteLabel;
        UIButton *websiteLabel_Button;
        
        if([phArray count] ==1){
            websiteLabel  = [[UILabel alloc]initWithFrame:CGRectMake(60, 35, 200, 30)];
            websiteLabel_Button = [[UIButton alloc]initWithFrame:CGRectMake(60, 35, 200, 30)];}
        else{
            websiteLabel  = [[UILabel alloc]initWithFrame:CGRectMake(60,65, 200, 30)];
            websiteLabel_Button = [[UIButton alloc]initWithFrame:CGRectMake(60, 65, 200, 30)];                                                                          }
        [websiteLabel_Button addTarget:self
                                action:@selector(websiteButtonpressed:)
                      forControlEvents:UIControlEventTouchUpInside];
        
        websiteLabel.text = thisProduct.websiteLink;
        
        websiteLabel.numberOfLines = 0;
        websiteLabel.font = [UIFont systemFontOfSize:15.0];
        [contactsView addSubview:websiteLabel];
        [contactsView addSubview:websiteLabel_Button];
        
        UIButton *websiteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [websiteButton setImage:[UIImage imageNamed:@"globe.png"] forState:UIControlStateNormal];
        [websiteButton addTarget:self
                          action:@selector(websiteButtonpressed:)
                forControlEvents:UIControlEventTouchUpInside];
        if([phArray count] ==1)
            websiteButton.frame = CGRectMake(30.0,40.0, 25.0, 25.0);
        
        else
            websiteButton.frame = CGRectMake(30.0,70.0, 25.0, 25.0);
        [contactsView addSubview:websiteButton];
        
    }
    
    
    
    
    
    
    
    //    NSString *lGT = @"<br>";//@"&lt;br&gt;";
    //    NSString *bold = @"<b>";//@"&lt;b&gt;";
    //    NSString *lGTE = @"</br>";//@"&lt;/br&gt;";
    //    NSString *boldE = @"</b>";//@"&lt;/b&gt;";
    //    NSString *contactStr = [NSString stringWithFormat:@"%@%@%@,%@%@ %@,%@ %@%@,%@ %@%@,%@ %@%@,%@ %@%@.%@",bold,thisProduct.contactMerchantName,boldE,lGT,thisProduct.contactFirstName,thisProduct.contactlastName,lGTE,lGT,thisProduct.contactSuburb,lGTE,lGT,thisProduct.contactState,lGTE,lGT,thisProduct.contactPostcode,lGTE,lGT,thisProduct.contactCountry,lGTE];
    //    UIWebView *offerWebView;
    //
    //    if ((contactsView.subviews.count != 0) ) {
    //        offerWebView = (UIWebView *)[contactsView viewWithTag:2013];
    //
    //    }
    //    else {
    //        offerWebView = [[UIWebView alloc] initWithFrame:contactsView.bounds];
    //    }
    //
    //
    //    [offerWebView loadHTMLString:contactStr baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    //    offerWebView.tag = 1111;
    //    offerWebView.delegate = self;
    //    [contactsView addSubview:offerWebView];
    
}

- (void) updateAddressView {
    
    [self fetchMerchantWithProductID:thisProduct.productId];
    
}

- (void) fetchMerchantWithProductID:(NSString *) productId {
    
    if (merchantFetchRequest) {
        // Already a request is in progress.
        return;
    }
    
    if(addressDetailsLoaded) {
        // Already address is processed.
        return;
    }
    
    // Make server call for more products.
    NSString *urlString = [NSString stringWithFormat:@"%@get_product_addresses.php?pid=%@",URL_Prefix,productId];
    
    NSLog(@"Fetch Merchants URL: %@",urlString);
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    merchantFetchRequest = [ASIFormDataRequest requestWithURL:url];
    [merchantFetchRequest setDelegate:self];
    [merchantFetchRequest startAsynchronous];
    
    
}


-(NSString *)appendStringWithNewline:(NSString *)str
{
    NSString *appendStr = [NSString stringWithFormat:@"\n%@",str];
    return appendStr;
}

- (IBAction) offerButtonPressed:(id) sender {
    
    [self bringForwardTab:sender];
    
    if (!offerView) {
        offerView = [[UIView alloc] initWithFrame:mainContentHolder.bounds];
        offerView.backgroundColor = [UIColor whiteColor];
        
        //offerView.layer.cornerRadius = 5.0;
        offerView.layer.borderColor = [UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0].CGColor;
        offerView.layer.borderWidth = 2.0;
    }
    
    [self cleanMainController];
    
    [self updateOfferView];
    
    [self addOfferViewAsSubview];
    [self updateUIWithProductDetails:thisProduct];
}

- (IBAction) contactButtonPressed:(id) sender {
    
    [self bringForwardTab:sender];
    
    if (!contactsView) {
        contactsView = [[UIView alloc] initWithFrame:mainContentHolder.bounds];
        contactsView.backgroundColor = [UIColor whiteColor];
        
        //contactsView.layer.cornerRadius = 5.0;
        contactsView.layer.borderColor = [UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0].CGColor;
        contactsView.layer.borderWidth = 2.0;
        
     /*   UIButton *callButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [callButton addTarget:self
                   action:@selector(callButtonpressed:)
         forControlEvents:UIControlEventTouchUpInside];
        
        callButton.frame = CGRectMake(30.0, 100.0, 25.0, 25.0);
        [contactsView addSubview:callButton];
        
        UILabel *contactsLabel  = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, theSize.width, theSize.height)];
        contactsLabel.text = thisProduct.phone;
        
        
        contactsLabel.numberOfLines = 0;
        contactsLabel.font = [UIFont systemFontOfSize:15.0];
        [contactsView addSubview:contactsLabel];*/
        
        
    }
    
    [self cleanMainController];
    [self updateContactsView];
    [self addContactsViewAsSubview];
    
}

-(void)callButtonpressed:(id)sender
{
    NSLog(@"call button pressed");
    
    
    NSString *callNumber1 =@"";
   NSLog(@"%@",phArray);
    
    if([sender tag]==1){
        callNumber1 = [phArray objectAtIndex:0];
    }
    else if([sender tag] == 2)
    {
        callNumber1 = [phArray objectAtIndex:1];
    }
    
    UIAlertView *call_Alert = [[UIAlertView alloc]initWithTitle:@"Call" message:callNumber1 delegate:self cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel" , nil];
    call_Alert.tag = 111;
    [call_Alert show];
    
    
}
-(void)websiteButtonpressed:(id)sender
{
    NSLog(@"==website button pressed");
    NSURL *websiteUrl = [NSURL URLWithString:thisProduct.websiteLink];
    
    [[UIApplication sharedApplication] openURL:websiteUrl];
}
- (IBAction) addressButtonPressed:(id) sender {
    
    [self bringForwardTab:sender];
    
    if (!addressView) {
        addressView = [[UIView alloc] initWithFrame:mainContentHolder.bounds];
        addressView.backgroundColor = [UIColor whiteColor];
        
        //addressView.layer.cornerRadius = 5.0;
        addressView.layer.borderColor = [UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0].CGColor;
        addressView.layer.borderWidth = 2.0;
    }
    if(!merchantTableView)
    {
        merchantTableView = [[UITableView alloc]initWithFrame:mainContentHolder.bounds style:UITableViewStylePlain];
        merchantTableView.delegate = self;
        merchantTableView.dataSource = self;
        [addressView addSubview:merchantTableView];
    }
    
    [self cleanMainController];
    [self updateAddressView];
    [self addAddressViewAsSubview];
    
}


- (IBAction) mapButtonPressed:(id) sender
{
    NSLog(@"=====MAPBUTTON PRESSED=====");
    
  //  NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=newyork"];
    
    NSLog(@"Tag value is..... %d",[sender tag]);
    Merchant *aMerchant = [merchantList objectAtIndex:[sender tag]];
    Location.latitude = aMerchant.coordinate.latitude;
    Location.longitude = aMerchant.coordinate.longitude;
    NSLog(@"latitude and longitude is.. %f,%f",Location.latitude,Location.longitude);


    
    NSURL *addressUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com?q=%f,%f",Location.latitude, Location.longitude, nil]];
   // NSLog(@"addressUrl  is ....%@",addressUrl);
    
    [[UIApplication sharedApplication] openURL:addressUrl];
}

//get_product.php

- (void) fetchProductDetails:(id) sender {
    
}


- (void) updateUIWithProductDetails:(Product *) aProduct {
    
     NSLog(@"** PRODUCT ** updateUIWithProductDetails");
    thisProduct = aProduct;
    
    if (thisProduct.productImage) {
        [self.imageView setImage:thisProduct.productImage];
        NSLog(@"image Available in Product");
    }
    
    
    
    else {
        NSLog(@"image Not Available");
        NSLog(@"thisProduct.productImgLink: %@",thisProduct.productImgLink);
        imageRequest = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:thisProduct.productImgLink]];
        imageRequest.delegate = self;
        [self.imageActivityIndicator startAnimating];
        [imageRequest startAsynchronous];
    }  
    
   
    [self updateOfferView];
    
   
    
}


#pragma WebViewDelegate methods

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
   // [loadingView setHidden:YES];
    if (webView.tag == 2012) {
        
        UIWebView *offerWebView = webView;
        
        CGRect modifiedFrame = offerWebView.frame;
        modifiedFrame.size.height = offerWebView.scrollView.contentSize.height;
        offerWebView.frame = modifiedFrame; 
        
      //  webView.scalesPageToFit=YES;
      //  webView.multipleTouchEnabled=YES;
        
        NSLog(@"webview frame = %@, webview content size = %@",NSStringFromCGRect(offerWebView.frame),NSStringFromCGSize(offerWebView.scrollView.contentSize));
        
        CGRect modifiedOfferViewFrame = offerView.frame;
        modifiedOfferViewFrame.size = offerWebView.frame.size;
        offerView.frame = modifiedOfferViewFrame; 
        
        if ([offerView superview]) {
            [self addOfferViewAsSubview];
        }
        
    }
    
}

-(BOOL)webView:(UIWebView *)descriptionTextView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (UIWebViewNavigationTypeLinkClicked == navigationType) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}

#pragma mark -- ASIHttpRequestDelegate methods

- (void) showNoImage {
    imageView.layer.borderWidth = 2.0;
    imageView.layer.borderColor = [UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0].CGColor;
    imageView.layer.cornerRadius = 4.0;
    
    self.noImageLabel.textColor = [UIColor colorWithRed:50/255.0 green:152/255.0 blue:223/255.0 alpha:1.0];
    self.noImageLabel.hidden = NO;
}


- (void)requestFailed:(ASIHTTPRequest *)request {
    
    if (request == imageRequest) {
        imageRequest = nil;
        [self.imageActivityIndicator stopAnimating];
        //UIImage *image = [UIImage imageWithData:[request responseData]];
        //[self.imageView setImage:image];
        [self showNoImage];
    }
    else if(request == merchantFetchRequest) {
        merchantFetchRequest = nil;
        NSLog(@"Product ** RES: %@",[request responseString]);
        
        NSXMLParser *merchantParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        merchantListParser = [[MerchantListParser alloc] init];
        merchantListParser.delegate = self;
        merchantParser.delegate = merchantListParser;
        [merchantParser parse];
        UIAlertView *failedAlert=[[UIAlertView alloc]initWithTitle:@"Failed!" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [failedAlert show];
    }
    
    else if(request == redeemRequest)
    {
        redeemRequest = nil;
        [self.redeemActivityIndicator stopAnimating];
        NSLog(@"REDEEM RESPONSE::: %@",[request responseString]);
        UIAlertView *failedAlert=[[UIAlertView alloc]initWithTitle:@"Failed!" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [failedAlert show];
    }
    
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    if (request == imageRequest) {
       // imageRequest = nil;
        [self.imageActivityIndicator stopAnimating];
        UIImage *image = [UIImage imageWithData:[request responseData]];
        
        if (image.size.width == 0 && image.size.height == 0) {
            NSLog(@"requestFinished -- :( There was NO image");
            [self showNoImage];
        }
        else {
             NSLog(@"requestFinished -- :Image Available");
            [self.imageView setImage:image];
            self.myCouponImage = image;
           
            
        }
    }
    else if(request == merchantFetchRequest) {
        NSLog(@"Merchant ** RES: %@",[request responseString]);
        
        NSXMLParser *merchantParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        merchantListParser = [[MerchantListParser alloc] init];
        merchantListParser.delegate = self;
        merchantParser.delegate = merchantListParser;
        [merchantParser parse];
        
        merchantFetchRequest = nil;
        addressDetailsLoaded = YES;
    }
    
    
    else if(request == redeemRequest)
    {
        redeemRequest = nil;
        [self.redeemActivityIndicator stopAnimating];
        NSLog(@"REDEEM RESPONSE::: %@",[request responseString]);
        
        NSString *msg = [NSString stringWithFormat:@"For redeeming this offer %@",thisProduct.productName];
        if([[request responseString] isEqualToString:@"success"])
        {
            
            UIAlertView *thanksAlert = [[UIAlertView alloc]initWithTitle:@"Thank You!" message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil, nil];
            thanksAlert.tag = 333;
            [thanksAlert show];
            
        }
    }
    
}

- (void)requestStarted:(ASIHTTPRequest *)request{
    
    NSLog(@" Image request started");
    
}

- (void)parsingMerchantListFinished:(NSArray *)merchantsListLocal {
    NSLog(@"\n Merchants lIst in  parsingDataFinished metnod = %d",[merchantsListLocal count]);
    
    if (!merchantList) {
        merchantList = [[NSMutableArray alloc] initWithArray:merchantsListLocal];
    }
    else {
        [merchantList addObjectsFromArray:merchantsListLocal];
    }
    
    [merchantTableView reloadData];
}

- (void)parsingMerchantListXMLFailed {
    
}


#pragma mark -- UITableView Datasource & Delegate methods

/** Datasource methods **/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSLog(@"\n MerchantsList count = %lu",(unsigned long)[merchantList count]);
        return [merchantList count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"cell";
    static NSString *loadingCellIdentifier = @"loadingCell";
    
    if (indexPath.row == merchantList.count) {
        
        UITableViewCell *loadingCell = [tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
        
        if (!loadingCell) {
            loadingCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:loadingCellIdentifier];
        }
        
        
        loadingCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return loadingCell;
        
    }
    
    
    MerchantAddressCell *cell = (MerchantAddressCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    /*  if (!cell) {
     
     NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"MerchantAddressCell" owner:nil options:nil];
     
     for (UIView *cellview in views) {
     if([cellview isKindOfClass:[UITableViewCell class]])
     {
     cell = (MerchantAddressCell *)cellview;
     }
     }
     
     }*/
    
    
    cell= (MerchantAddressCell *)[tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
    if(cell==nil)
    {
        
        cell =[[[NSBundle mainBundle] loadNibNamed:@"MerchantAddressCell" owner:self options:nil]objectAtIndex:0];
        
        
    }
    
    //    NSString *str;
    //
    //    if([thisProduct.contactSuburb length]>0 )
    //    {
    //        str = [str stringByAppendingString:[self appendStringWithNewline:thisProduct.contactSuburb]];
    //    }
    //    if([thisProduct.contactState length]>0 )
    //    {
    //        str = [str stringByAppendingString:[self appendStringWithNewline:thisProduct.contactState]];
    //    }
    //    if([thisProduct.contactPostcode length]>0 )
    //    {
    //        str = [str stringByAppendingString:[self appendStringWithNewline:thisProduct.contactPostcode]];
    //    }
    //    if([thisProduct.contactCountry length]>0 )
    //    {
    //        str = [str stringByAppendingString:[self appendStringWithNewline:thisProduct.contactCountry]];
    //    }
    //
    
    
    Merchant *aMerchant = [merchantList objectAtIndex:indexPath.row];
    
    NSLog(@"Address:%@",aMerchant.mMailAddress1);
    NSLog(@"Suburb:%@",aMerchant.mMailSuburb);
    NSLog(@"State:%@",aMerchant.mState);
    NSLog(@"Phone:%@",aMerchant.mPhone);
    
    if([aMerchant.mMailAddress1 length]<=0 && aMerchant.mState.length == 0){
        NSLog(@"null");
        
        cell.merchantAddress.text = @"No Addresses Available";
        cell.mapButton.hidden = YES;
        cell.mapLabel.hidden = YES;
    }
    else{
        
        NSString * str =aMerchant.mMailAddress1 ;
        
        if( [aMerchant.mMailSuburb isEqualToString:@"(null)"] )
        {// NSLog(@"Address: %@",aMerchant.mMailAddress1);
            // str=@"trimmed";
            
        }
        else
        {
            NSLog(@"null: %@",[NSNull null]);
            
            str = [str stringByAppendingString:[self appendStringWithNewline:aMerchant.mMailSuburb]];
        }
        if([aMerchant.mState length]>0 && [aMerchant.mPostCode length]>0)
            str = [NSString stringWithFormat:@"%@, %@-%@",str,aMerchant.mState,aMerchant.mPostCode];
        else if([aMerchant.mState length]>0)
            str = [NSString stringWithFormat:@"%@, %@",str,aMerchant.mState];
        if([aMerchant.mPhone length]>0 ){
            NSString *phno = [NSString stringWithFormat:@"Ph: %@",aMerchant.mPhone];
            str = [str stringByAppendingString:[self appendStringWithNewline:phno]];
        }
        // NSString *deviceName = @"Kenny's iPhone";
        NSLog(@"non--stripped1 is...%@ ",str  );
        NSString *stripped = [str stringByReplacingOccurrencesOfString:@"(null)," withString:@""];
        NSString *stripped1 = [stripped stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
        //[stripped stringByReplacingOccurrencesOfString:@"," withString:@""];
        
        
        
        //  NSLog(@"stripped1 is...%@ ",stripped  );
        
        
        //NSString* result = [[stripped  componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        // NSLog(@"stripped fnl is...%@ ",result );
        cell.merchantAddress.text = stripped1;
        
    }
    cell.merchantAddress.textColor = [UIColor blackColor];
    Location.latitude = aMerchant.coordinate.latitude;
    Location.longitude = aMerchant.coordinate.longitude;
    NSLog(@"latitude and longitude is.. %f,%f",Location.latitude,Location.longitude);
    
    
    if ( (Location.latitude == 0.000000)|| (Location.longitude == 0.000000)|| [cell.merchantAddress.text isEqualToString: @"No Addresses Available"])
    {
        if (indexPath.row >= 1) {
            cell.merchantAddress.text = @"";
        }
        cell.mapButton.hidden = YES;
        cell.mapLabel.hidden = YES;
        
        
    }
    else
    {
        cell.mapButton.hidden = NO;
        cell.mapLabel.hidden = NO;
        
    }
    cell.mapButton.tag = indexPath.row;
    
    // cell.productOfferLabel.text = aMerchant.mMailAddress1;
    // cell.productOfferLabel.textColor = [UIColor blackColor];
    // cell.imgLoadingIndicator.hidesWhenStopped = YES;
    
    
    //    int rowModuleNo = indexPath.row % 4;
    //
    //    if (rowModuleNo == 0) {
    //        cell.headerContainerView.backgroundColor = [UIColor colorWithRed:220/255.0 green:50/255.0 blue:100/255.0 alpha:1.0];
    //    }
    //    else if (rowModuleNo == 1) {
    //        cell.headerContainerView.backgroundColor = [UIColor colorWithRed:120/255.0 green:150/255.0 blue:160/255.0 alpha:1.0];
    //    }
    //    else if (rowModuleNo == 2) {
    //        cell.headerContainerView.backgroundColor = [UIColor colorWithRed:20/255.0 green:150/255.0 blue:200/255.0 alpha:1.0];
    //    }
    //    else if (rowModuleNo == 3) {
    //        cell.headerContainerView.backgroundColor = [UIColor colorWithRed:170/255.0 green:220/255.0 blue:100/255.0 alpha:1.0];
    //    }
    //    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return ProductCellHeight;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    [self.navigationController popViewControllerAnimated:YES];
//}



/*
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
   // NSLog(@"didUpdateToLocation: %@", newLocation);
    self.userLocation = [newLocation coordinate];
   // NSLog(@"lattitude::%f",self.userLocation.latitude);
   // NSLog(@"longitude::%f",self.userLocation.longitude);
    
    
    
}

*/



@end
