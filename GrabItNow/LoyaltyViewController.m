//
//  LoyaltyViewController.m
//  CoffeeApp
//
//  Created by vairat on 27/03/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import "LoyaltyViewController.h"
#import "AppDelegate.h"
#import "LoyaltyDetailViewController.h"
#import "ASIFormDataRequest.h"
#import "LoyalityTableViewCell.h"
#import "Product.h"

@interface LoyaltyViewController (){
    
    AppDelegate *appDelegate;
    ProductListParser  *productsXMLParser;
    UIImage *fetchedProductImage;
    Product *pro;
    
    NSMutableDictionary *staticImageDictionary;
}
@property(nonatomic, strong)UIImage *fetchedProductImage;
@end


@implementation LoyaltyViewController
@synthesize loyaltyTableView, productsList, productImagesArray,fetchedProductImage;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    appDelegate = (AppDelegate *) [UIApplication sharedApplication].delegate;
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    productImagesArray = [[NSMutableArray alloc]init];

    
    NSString *urlString =@"http://184.107.152.53/newapp/get_loyalty_offers.php?cid=24";
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIFormDataRequest *productFetchRequest = [ASIFormDataRequest requestWithURL:url];
    [productFetchRequest setDelegate:self];
    [productFetchRequest startAsynchronous];
   
    
}
- (void) viewWillAppear:(BOOL)animated
{
    [UIView setAnimationsEnabled:YES];
    
    [appDelegate.homeViewController hideBackButon:YES];
    [appDelegate.homeViewController setHeaderTitle:@"HOT OFFERS"];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

#pragma mark- RequestFinished and RequestFailed

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
    productsXMLParser = [[ProductListParser alloc] init];
    productsXMLParser.delegate = self;
    productsParser.delegate = productsXMLParser;
    [productsParser parse];
    
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                         message:@"Server Busy"
                                                        delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil];
    [alert_view show];
    
    
}


#pragma mark- TableView DataSource and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [productsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *cellIdentifier = @"loyaltyDetailCell";
    
    LoyalityTableViewCell *cell;
    
    if (cell == nil)
        cell = (LoyalityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    cell.tag = indexPath.row;
    pro = [productsList objectAtIndex:indexPath.row];
    cell.lblTitle.text = pro.productName;
    cell.lblSubTitle.text = pro.productOffer;
    

    
    NSString *imageUrl = [NSString stringWithFormat:@"%@", pro.carouselImgLink];
    // Here we either load from the web or we cache it...
    UIImage *image = [self imageNamed:imageUrl cache:YES];
    
    NSLog(@"productImage is %@ ",image);
    // NSLog(@"staticImageDictionary is %@ ",staticImageDictionary);
    cell.product_ImageView.image = nil;
    cell.product_ImageView.image=image;
    
    /*dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:pro.carouselImgLink]];
        
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (cell.tag == indexPath.row) {
                    cell.product_ImageView.image = image;
                    //[productImagesArray addObject:image];
                   // NSLog(@"count of product image is %d ",productImagesArray.count);
                    [cell setNeedsLayout];
                }
            });
        }
    });*/
    
    
    int rowModuleNo = indexPath.row % 2;
    
    if (rowModuleNo == 0)
        cell.backgroundColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0];
    
    else if (rowModuleNo == 1)
        cell.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1.0];

    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
}

#pragma mark- storing images in NSdictionary

- (UIImage*)imageNamed:(NSString*)imageNamed cache:(BOOL)cache
{
    UIImage* retImage = [staticImageDictionary objectForKey:imageNamed];
    
    if (![self validateUrl:imageNamed])
    {
        
        retImage= [UIImage imageNamed:@"no_image_Available"];
        
    }
    else
    {
        //UIImage* retImage = [staticImageDictionary objectForKey:imageNamed];
        if (retImage == nil)
        {
            retImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageNamed]]];
            if (cache)
            {
                if (staticImageDictionary == nil)
                    staticImageDictionary = [NSMutableDictionary new];
                
                [staticImageDictionary setObject:retImage forKey:imageNamed];
            }
        }
    }
    return retImage;
}

- (BOOL) validateUrl: (NSString *) candidate
{
    NSString *urlRegEx =
    @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
}



#pragma mark- Calling Parsing Methods
- (void)parsingProductListFinished:(NSArray *)prodcutsListLocal
{
    NSLog(@"product name %@",prodcutsListLocal);
    
    if (!productsList)
        
        productsList = [[NSMutableArray alloc] initWithArray:prodcutsListLocal];
    
    else
        
        [productsList addObjectsFromArray:prodcutsListLocal];
    
    
    [loyaltyTableView reloadData];
    
    
}

- (void)parsingProductListXMLFailed
{
    //NSLog(@"Product list updated failed ");
}





#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSLog(@"================------------");
   // if ([segue.identifier isEqualToString:@"loyaltyDetail"])
   // {
        NSIndexPath *indexPath = [self.loyaltyTableView indexPathForSelectedRow];
    
    LoyalityTableViewCell *cell = (LoyalityTableViewCell *)[loyaltyTableView cellForRowAtIndexPath:indexPath];
    self.fetchedProductImage = cell.product_ImageView.image;
    pro = [productsList objectAtIndex:indexPath.row];
        
    
        
        LoyaltyDetailViewController *loyaltyDetailController = segue.destinationViewController;
         
        loyaltyDetailController.product_id   = pro.productId;
        loyaltyDetailController.product_Name = pro.productName;
        loyaltyDetailController.productImage = self.fetchedProductImage;
        loyaltyDetailController.product_Offer= pro.productOffer;
    
   // }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
