//
//  Category.h
//  GrabItNow
//
//  Created by MyRewards on 12/22/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Category : NSObject
{
    NSString *catId;
    NSString *catName;
    NSString *catDescription;
    NSString *catImgURLString;
}

@property (nonatomic, strong) NSString *catId;
@property (nonatomic, strong) NSString *catName;
@property (nonatomic, strong) NSString *catDescription;
@property (nonatomic, strong) NSString *catImgURLString;

@end

/*
<category>
    <id>10</id>
    <parent_id>0</parent_id>
    <name>Automotive</name>
    <description>Generic Automotive</description>
    <details></details>
    <sort>0</sort>
    <deleted>0</deleted>
    <created>0000-00-00 00:00:00</created>
    <modified>2012-02-07 17:51:24</modified>
    <countries>Australia</countries>
    <logo_extension>jpg</logo_extension>
    <search_weight>1</search_weight>
 </category>
*/