//
//  CollectionViewManager.h
//  PDKTStickySectionHeadersCollectionViewLayoutDemo
//
//  Created by Daniel García on 31/12/13.
//  Copyright (c) 2013 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDKTStickySectionHeadersCollectionViewLayout.h"

#import "ProductDataParser.h"
#import "RedeemXMLparser.h"
#import "MerchantListParser.h"
#import "AddressCell.h"

@interface CollectionViewManager : NSObject<UICollectionViewDataSource,UICollectionViewDelegate,PDKTStickySectionHeadersCollectionViewLayoutDelegate,ProductXMLParserDelegate,RedeemXMLparserDelegate, MerchantXMLParserDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
{
    int phnoCount;
    NSMutableArray *phMutableArray;
    NSArray *phArray;
    Product *OProduct;
    CLLocationCoordinate2D Location;
    
}

@property (weak,   nonatomic) UICollectionView *collectionView;
@property (strong, nonatomic) NSString *product_ID;
@property (strong, nonatomic) AddressCell *cCell;
@property (nonatomic, strong) UIImage *proImage;
@property (strong, nonatomic) NSString *selectedStampCount;

@property (strong, nonatomic) NSString *redeem_psw;
@property (strong, nonatomic) UIButton *redeem_Button;
@property (readwrite, nonatomic)int stampTapCount;

@property (readwrite, nonatomic)int redeem_Target;
@property (readwrite, nonatomic)float checkedStamps;

@property (strong, nonatomic) Product *OProduct;
@property (strong, nonatomic) IBOutlet UIButton *freebiesButtonCM;
@property (strong,nonatomic) UIButton *favouriteButton;

-(void)redeemButtonPressed;

@end
