//
//  LoyaltyDetailViewController.m
//  CoffeeApp
//
//  Created by vairat on 28/03/15.
//  Copyright (c) 2015 Vairat. All rights reserved.
//

#import "LoyaltyDetailViewController.h"
#import "CollectionViewManager.h"
#import "KeyboardViewController.h"
#import "AppDelegate.h"
#import <AdSupport/ASIdentifierManager.h>
#import "ASIFormDataRequest.h"
#import "FreebiesViewController.h"

@interface LoyaltyDetailViewController (){
    
    AppDelegate *appDelegate;
    BOOL controlCameFromKeyboardView;
    BOOL isLastStamp;
    
    
    ASIFormDataRequest *getProductRequest;
    ASIFormDataRequest *merchantFetchRequest;
    ProductDataParser *productDataXMLParser;
    ASIFormDataRequest *freebiesRequest;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong,nonatomic) CollectionViewManager *collectionViewManager;
@property (strong,nonatomic) KeyboardViewController *keyboardViewControllerObj;


@end

@implementation LoyaltyDetailViewController
@synthesize keyboardViewControllerObj;
@synthesize productImage, product_id, product_Name,redeemButton,FreebiesButton, product_Offer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        

    }
    return self;
}
- (BOOL)prefersStatusBarHidden
{
    return NO;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    
    self.title = @"Product Detail";
    UIBarButtonItem *flipButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Back"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(back)];
    self.navigationItem.leftBarButtonItem = flipButton;

    
    
    
    self.collectionViewManager                = [[CollectionViewManager alloc]init];
    self.collectionViewManager.product_ID     = product_id;
    self.collectionViewManager.proImage       = self.productImage;
    self.collectionViewManager.redeem_Button  = self.redeemButton;
    self.collectionViewManager.collectionView = self.collectionView;
    isLastStamp = NO;
    
    if(self.productImage)
        NSLog(@"self.productImage");
    else
        NSLog(@"no self.productImage");
    
   // NSLog(@"==========>>>>pro.productName::%@",pro.productName);
    NSLog(@"==========>>>>pro.productID::%@",self.collectionViewManager.product_ID);
   // NSLog(@"==========>>>>pro.productOffer::%@",pro.productOffer);
    
    

}
-(void)back{
    
    isLastStamp = NO;
    self.collectionViewManager.favouriteButton.hidden = YES;
    appDelegate.isFromFreebies = NO;
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(IBAction)presentFreebies:(id)sender
{
    UIStoryboard *storyboard = self.storyboard;
    FreebiesViewController *svc = [storyboard instantiateViewControllerWithIdentifier:@"freebiesViewController"];
//    [self presentViewController:svc animated:NO completion:nil];
    
    [self.navigationController pushViewController:svc animated:NO];

}
-(void) viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
    

    [appDelegate.homeViewController hideBackButon:NO];
    [appDelegate.homeViewController setHeaderTitle:@"PRODUCT DETAILS"];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
   
    
    self.collectionViewManager.stampTapCount = 0;
    self.collectionViewManager.selectedStampCount = [NSString stringWithFormat:@"1"];
    if (controlCameFromKeyboardView == YES) {
        
        controlCameFromKeyboardView = NO;
        self.collectionViewManager.favouriteButton.hidden = NO;
        
        [self.collectionViewManager redeemButtonPressed];
        
    }
    FreebiesButton = [[UIButton alloc]init];
    
    
    //contactlabel_Button=[UIButtonTypeCustom]
    // contactlabel_Button.tag = i;
    //  NSLog(@"contact btn tag %ld",(long)[contactlabel_Button tag]);
    //  [contactlabel_Button setBackgroundImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    //
    
    [FreebiesButton setBackgroundImage:[UIImage imageNamed:@"arrow.png"] forState:UIControlStateNormal];
    
    [FreebiesButton addTarget:self action:@selector(presentFreebies:)forControlEvents:UIControlEventTouchUpInside];
    
    NSLog(@"viewwillappear ");
    
    NSString *urlString = [NSString stringWithFormat:@"%@get_product.php?id=%@",URL_Prefix_Loyalty,product_id];
    NSLog(@"URL is %@ ",urlString);
    NSURL *url= [NSURL URLWithString:urlString];
    getProductRequest = [ASIFormDataRequest requestWithURL:url];
    [getProductRequest setDelegate:self];
    [getProductRequest startAsynchronous];


}

-(void)viewWillDisappear:(BOOL)animated
{
    _collectionViewManager.favouriteButton.hidden=YES;
    
    isLastStamp = NO;
    self.collectionViewManager.favouriteButton.hidden = YES;
    appDelegate.isFromFreebies = NO;
}


#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

- (IBAction)redeemBtnPressed:(id)sender {
    
    NSLog(@"--------------------->>>> %d",self.collectionViewManager.stampTapCount);
    self.collectionViewManager.stampTapCount = (self.collectionViewManager.stampTapCount == 0)?1:self.collectionViewManager.stampTapCount;
    self.collectionViewManager.selectedStampCount = [NSString stringWithFormat:@"%d",self.collectionViewManager.stampTapCount];
    NSLog(@"--------------------->>>> %@",self.collectionViewManager.selectedStampCount);
    self.collectionViewManager.freebiesButtonCM=FreebiesButton;
    
    if(self.collectionViewManager.checkedStamps+self.collectionViewManager.stampTapCount+1 == self.collectionViewManager.redeem_Target || self.collectionViewManager.stampTapCount == self.collectionViewManager.redeem_Target -1)
    {
        
        self.collectionViewManager.selectedStampCount = [NSString stringWithFormat:@"%d",self.collectionViewManager.stampTapCount+1];
        NSLog(@"case 4 : checkedStamps+1 == [currentProduct.redeemTarget integerValue]-1");
        
        isLastStamp = YES;
        
    }
    
    
    
    KeyboardViewController *kvc = [self.storyboard   instantiateViewControllerWithIdentifier:@"KeyboardViewController"];
    
    
    kvc.product_ID    = product_id;
    kvc.product_Image = productImage;
    kvc.product_Name  = product_Name;
    kvc.product_Offer = product_Offer;
    kvc.deviceID = [self deviceUDID];
    kvc.isLastStamp = isLastStamp;
    kvc.redeemPassword = self.collectionViewManager.redeem_psw;
    kvc.multipleRedeemCount = self.collectionViewManager.selectedStampCount;
    kvc.isFromFreebies=NO;
    NSLog(@"self.collectionViewManager.selectedStampCount:: %@",self.collectionViewManager.selectedStampCount);
    controlCameFromKeyboardView = YES;
    
    [self.navigationController pushViewController:kvc animated:NO];

}

-(NSString *)deviceUDID
{
    
    NSString *uid;
    if([ [ UIScreen mainScreen ] bounds ].size.height == 568)
    {
        uid= [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        
    }
    else
    {
        uid = [self advertisingIdentifier];
        if ([uid isKindOfClass:NULL]) {
            uid = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
        }
        
    }
    
    
    return uid;
}
- (NSString *) advertisingIdentifier
{
    if (!NSClassFromString(@"ASIdentifierManager")) {
        SEL selector = NSSelectorFromString(@"uniqueIdentifier");
        if ([[UIDevice currentDevice] respondsToSelector:selector]) {
            return [[UIDevice currentDevice] performSelector:selector];
        }
        
    }
    return [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
