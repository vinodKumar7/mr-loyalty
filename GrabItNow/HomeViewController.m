//
//  HomeViewController.m
//  GrabItNow
//
//  Created by MyRewards on 11/24/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//
#import <Parse/Parse.h>
#import <QuartzCore/QuartzCore.h>
#import "CustomBadge.h"
#import "HomeViewController.h"
#import "SearchViewController.h"
#import "DailyDealsViewController.h"
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"
#import "User.h"
#import "ProductListViewController.h"
#import "MyCardViewController.h"
#import "NearByViewController.h"
#import "HelpViewController.h"
#import "NoticeBoardViewController.h"
#import "MyParkerTimerViewController.h"
#import "ScannerViewController.h"
//#import "SendAFriendViewController.h"
#import "MenuTableCell.h"
#import "LoyaltyViewController.h"
#import "FreebiesViewController.h"


#define NearByTitle @"What's Around Me"
#define MyCardTitle @"My Card"
#define HotOffersTitle @"Hot Offers"
#define MyFavoritesTitle @"My Favorites"
#define DailyDealTitle @"Daily Deal"
#define HelpTitle @"Help"
#define SearchTitle @"Search"
#define NoticeBoardTitle @"Notice Board"
#define PTTitle @"ParkingTimer"
#define SendAFrndTitle @"Send A Friend";

#define SearchButtonTag 1
#define NearByButtonTag 2
#define MyCardButtonTag 3
#define HotOffersButtonTag 4
#define MyFavoritesButtonTag 5
#define DailyDealButtonTag 6
#define HelpButtonTag 7
#define NoticeBoardButtonTag 8
#define ParkingTimerButtonTag 9
#define SendAFriendTag 10

@interface HomeViewController ()
{
    UIViewController *activeController;
    
    UIView *blackTransparentView;
    BOOL showingMenuView;
    
    SearchViewController *searchController;
    ProductListViewController *myFavoritesViewController;
    ProductListViewController *offersViewController;
    LoyaltyViewController *loyaltyViewController;
    MyCardViewController *myCardViewController;
    NearByViewController *nearByViewController;
    HelpViewController *helpPageController;
    DailyDealsViewController *dailyDealController;
    NoticeBoardViewController *noticeViewController;
    MyParkerTimerViewController *ParkingtimerViewController;
    ScannerViewController       *scannerViewController;
    FreebiesViewController *freebiesViewController;
    
    
    AppDelegate *appDelegate;
    
    ASIFormDataRequest *bannerFetchRequest;
    ASIFormDataRequest *noticeBoardIDsRequest;
    
    UINavigationController *navController;
    
    NSMutableDictionary *menuButtonDetailsDictionary;
    UIView *badgeView;
    MenuItem currentMenuItem;
    int currentMenuItemTag;
    
    NSArray *menuItemsArray;
    NSArray *menuItemsImageNamesArray;
    
    NSIndexPath * currentSelectedIndexPath;
    CustomBadge *customBadge;
    
    NoticeBoardDataParser *NoticeBoardXMLParser;
    NSMutableArray *noticeIDArray;
    NSString *noticeBoardCount;
    int NCount;
    
    BOOL throughTableCellPressed;
}

@property (nonatomic, strong) NSIndexPath * currentSelectedIndexPath;
@property (nonatomic, strong) NSArray *menuItemsArray;
@property (nonatomic, strong) NSArray *menuItemsImageNamesArray;
@property (nonatomic, strong) SearchViewController *searchController;
@property (nonatomic, strong) ProductListViewController *myFavoritesViewController;
@property (nonatomic, strong) ProductListViewController *offersViewController;
@property (nonatomic, strong) LoyaltyViewController *loyaltyViewController;
@property (nonatomic, strong) MyCardViewController *myCardViewController;
@property (nonatomic, strong) NearByViewController *nearByViewController;
@property (nonatomic, strong) HelpViewController *helpPageController;
@property (nonatomic, strong) DailyDealsViewController *dailyDealController;
@property (nonatomic, strong) NoticeBoardViewController *noticeViewController;
@property (nonatomic, strong) ParkingTimer *ParkingtimerViewController;
@property (nonatomic, strong) FreebiesViewController *freebiesViewController;
@property (nonatomic, strong) UINavigationController *navController;

@property (nonatomic, strong) NSMutableDictionary *menuButtonDetailsDictionary;

@property (nonatomic) BOOL isReading;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, strong) UIView *qrSubView;

- (void) fetchBannerView;
- (void) presentNearByProductsController;
- (void) presentMyfavoritesProductsController;
- (void) presentOfferProductsController;
- (void) presentMyCardController;
- (void) presentdialyDealController;
- (void) presentNoticeBoardController;
- (void) presentParkingTimerController;
- (void) presentQRcodeScannerController;

@end

@implementation HomeViewController

@synthesize currentSelectedIndexPath;
@synthesize menuItemsArray;
@synthesize menuItemsImageNamesArray;

@synthesize menuTable;

@synthesize navController;
@synthesize searchController;
@synthesize nearByViewController;
@synthesize myFavoritesViewController;
@synthesize offersViewController;
@synthesize myCardViewController;
@synthesize helpPageController;
@synthesize dailyDealController;
@synthesize noticeViewController;
@synthesize loyaltyViewController;
@synthesize freebiesViewController;


@synthesize menuButtonDetailsDictionary;

@synthesize headerLabel;

@synthesize menuView;
@synthesize headerView;
@synthesize bannerView;
@synthesize menuButton;
@synthesize bannerImageView;
@synthesize menuTitleString;

@synthesize backButton;

@synthesize menuButton1;
@synthesize menuButton2;
@synthesize menuButton3;
@synthesize menuButton4;
@synthesize menuButton5;
@synthesize menuButton6,captureSession,qrSubView,button,isReading
;

@synthesize currentTextfield;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    //============== PUSH NOTIFICATION CODE
   // PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
   // [testObject setObject:@"bar" forKey:@"foo"];
  //  [testObject save];
    
    //===========
    if (self) {
        // Custom initialization
        showingMenuView = NO;
        noticeBoardCount = @"";
        NCount = 0;
        throughTableCellPressed = NO;
        currentSelectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        menuButtonDetailsDictionary = [[NSMutableDictionary alloc] init];
        menuItemsArray = [[NSArray alloc] initWithObjects:@"Search By Categories",@"What's Around Me",@"Hot Offers",@"My Favourites",@"Daily Deals",@"QR Code Reader",@"Notice Board",@"My Freebies",@"Log Out", nil];
        
        menuItemsImageNamesArray = [[NSArray alloc] initWithObjects:@"menu_search.png",@"menu_around_me.png",@"menu_hot_offers.png",@"menu_my_favourites.png",@"menu_daily_deals.png",@"menu_scan",@"menu_notice_board.png",@"menu_freebie.png",@"menu_logout.png", nil];
    }
    return self;
}

- (void)dealloc {
    
    searchController = nil;
    
    if (bannerFetchRequest) {
        bannerFetchRequest.delegate = nil;
        [bannerFetchRequest cancel];
        bannerFetchRequest = nil;
    }
    
}

- (void) hideBackButon:(BOOL) hide {
    backButton.hidden = hide;
}

- (void) setHeaderTitle:(NSString *) headerTitle {
    
    menuButton.hidden = NO;
    menuTitleString = headerTitle;
    self.headerLabel.text = headerTitle;
    [self.headerLabel setFont:[UIFont fontWithName:@"MyriadPro-Semibold" size:17]];
    
    if ([self.headerLabel.text isEqualToString:@"PRODUCT DETAILS"] || [self.headerLabel.text isEqualToString:@"MyRewards"]) {
        menuButton.hidden = YES;
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    headerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"headerView.png"]];
    
    // Apply gradient to the header
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = headerView.bounds;
    // gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:223/255.0 green:85/255.0 blue:49/255.0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:255/255.0 green:150/255.0 blue:100/255.0 alpha:1.0] CGColor], nil];
    [headerView.layer insertSublayer:gradient atIndex:0];
    
    [menuView removeFromSuperview];
    
    bannerView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
    bannerView.layer.shadowOpacity = 1.0;
    bannerView.layer.shadowOffset = CGSizeMake(0.0, 5.0);
    bannerView.layer.shadowRadius = 5.0;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
        
        
        CGRect backFrame = self.backButton.frame;
        backFrame.origin.y = self.backButton.frame.origin.y + 7;
        self.backButton.frame = backFrame;
        
        CGRect menuFrame = self.menuButton.frame;
        menuFrame.origin.y = self.menuButton.frame.origin.y + 7;
        self.menuButton.frame = menuFrame;
        
        CGRect headerLabelFrame = self.headerLabel.frame;
        headerLabelFrame.origin.y = self.headerLabel.frame.origin.y + 7;
        self.headerLabel.frame = headerLabelFrame;
        
    }
    
    
    
    
    
    [self presentSearchController];
    
    // Fetch Banner View
    // [self fetchNoticeBoardItemsCount];
    [self fetchBannerView];
    
    
    
    [self performSelector:@selector( menuButtonTapped:) withObject:menuButton afterDelay:0.7];
    
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - General Methods

- (IBAction)backButtonTapped:(id)sender {
    [navController popViewControllerAnimated:YES];
    [currentTextfield resignFirstResponder];
}

- (IBAction) menuButtonTapped:(id) sender {
    
    NSLog(@"menuButtonTapped");
    
    [UIView setAnimationsEnabled:YES];
    
    [currentTextfield resignFirstResponder];
    if (showingMenuView) {
        // Remove the menuView
        //NSLog(@"Remove Menu view");
        showingMenuView = NO;
        NSLog(@"menu throughTableCellPressed::%d",throughTableCellPressed);
        if (appDelegate.isNoticeBoardBground && throughTableCellPressed == NO) {
            NSLog(@"============NoticeBoardViewController is in BG================");
            throughTableCellPressed = NO;
            NoticeBoardViewController *noticeVC =  (NoticeBoardViewController *)[appDelegate.refArray objectAtIndex:0];
            [noticeVC viewWillAppear:YES];
        }
        
        [self removeBlackTransparentView];
        [UIView animateWithDuration:0.5 delay:0.0 options:
         UIViewAnimationOptionAllowAnimatedContent animations:^{
             CGRect menuViewFrame = menuView.frame;
             // menuViewFrame.origin.x = 20;
             menuViewFrame.origin.y = -(menuViewFrame.size.height - (headerView.frame.size.height ));
             self.headerLabel.text = menuTitleString;
             menuView.frame = menuViewFrame;
             
         } completion:^(BOOL finished) {
             throughTableCellPressed = NO;
             NSLog(@"after animation throughTableCellPressed::%d",throughTableCellPressed);
             [menuView removeFromSuperview];
         }];
        
    }
    else {
        
        if ([menuView superview]) {
            // This means menu closing/opening animation is happening.
            // Do nothing here
            return;
        }
        [self fetchNoticeBoardItemsCount];
        //NSLog(@"Show Menu view");// Show Menu view
        // [self calculatingUnreadNotices];
        [self showBlackTransparentView];
        
        
        [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
            CGRect menuViewFrame = menuView.frame;
            menuViewFrame.origin.x = 0;
            menuViewFrame.origin.y = bannerView.frame.origin.y;
            menuViewFrame.size.height = self.view.frame.size.height - (bannerView.frame.origin.y );
            
            menuView.frame = menuViewFrame;
            self.headerLabel.text = @"MENU";
            
            
        } completion:^(BOOL finished) {
            showingMenuView = YES;
        }];
        
        [self.view insertSubview:menuView belowSubview:headerView];
        
    }
    
}

//- (void)viewWillLayoutSubviews {
//
//    if(blackTransparentView) {
//        if ([blackTransparentView superview]) {
//            blackTransparentView.frame = self.view.bounds;
//        }
//    }
//
//}

-(void)viewWillAppear:(BOOL)animated

{
    
    CGRect headerFrame = headerView.frame;
    headerFrame.origin.y = 0;
    headerView.frame = headerFrame;
    
    CGRect bannerFrame = bannerView.frame;
    bannerFrame.origin.y = headerFrame.size.height;
    bannerView.frame = bannerFrame;
    
    //menuView.backgroundColor=[UIColor blu];
}
- (void) showBlackTransparentView {
    CGRect transFrame = self.view.bounds;
    
    if (!blackTransparentView) {
        blackTransparentView = [[UIView alloc] initWithFrame:transFrame];
        blackTransparentView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    }
    
    blackTransparentView.frame = transFrame;
    blackTransparentView.alpha = 0;
    [self.view addSubview:blackTransparentView];
    [self.view bringSubviewToFront:headerView];
    [self.view bringSubviewToFront:bannerView];
    
    [UIView animateWithDuration:0.5 animations:^{
        blackTransparentView.alpha = 1.0;
    }];
}

- (void) removeBlackTransparentView {
    
    [UIView animateWithDuration:0.5 animations:^{
        blackTransparentView.alpha = 0.0;
    } completion:^(BOOL finished) {
        [blackTransparentView removeFromSuperview];
    }];
    
    
}


#pragma mark - Search Controller methods

- (void) updateTitleOfButton:(UIButton *) btn {
    
    NSString *btnTitle = @"";
    
    switch (btn.tag) {
        case SearchButtonTag:
            btnTitle = SearchTitle;
            break;
        case NearByButtonTag:
            btnTitle = NearByTitle;
            
            break;
        case MyCardButtonTag:
            btnTitle = MyCardTitle;
            break;
        case MyFavoritesButtonTag:
            btnTitle = MyFavoritesTitle;
            break;
        case HotOffersButtonTag:
            btnTitle = HotOffersTitle;
            break;
        case DailyDealButtonTag:
            btnTitle = DailyDealTitle;
            break;
        case HelpButtonTag:
            btnTitle = HelpTitle;
            break;
       
        default:
            break;
    }
    
    [btn setTitle:btnTitle forState:UIControlStateNormal];
}

- (void) updateMenuViewButtonTagsAndTitles {
    
    NSLog(@"updateMenuViewButtonTagsAndTitles");
    
    UIButton * currentButton = menuButton1;
    //  NSLog(@"currentMenuItemTag is.... %d",currentMenuItemTag);
    for (int i = SearchButtonTag; i <= ParkingTimerButtonTag; i++) // '8' is calculated on basis of total number of menu options
    {
        
        
        if (i != currentMenuItemTag) {
            
            // Update TAG of currentButton
            currentButton.tag = i;
            
            // Update TITLE of currentButton
            [self updateTitleOfButton:currentButton];
            
            
            // Now update "currentButton"
            if (currentButton == menuButton1) {
                currentButton = menuButton2;
            }
            else if (currentButton == menuButton2) {
                currentButton = menuButton3;
            }
            else if (currentButton == menuButton3) {
                currentButton = menuButton4;
            }
            else if (currentButton == menuButton4) {
                currentButton = menuButton5;
            }
            else if (currentButton == menuButton5) {
                currentButton = menuButton6;
            }
            else if (currentButton == menuButton6) {
                break;
            }
        }
    }
    
}

- (void) presentNewViewController:(UIViewController *) controller {
    
    
    
    [self updateMenuViewButtonTagsAndTitles];
    
    navController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    CGRect frame = controller.view.frame;
    frame.origin.y = bannerView.frame.origin.y;
    frame.size.height = self.view.frame.size.height - frame.origin.y;
    navController.view.frame =frame;
    
    [self.view addSubview:navController.view];
    
    [self.view bringSubviewToFront:headerView];
    [self.view bringSubviewToFront:bannerView];
}

- (void) presentSearchController {
    
    if (!searchController) {
        searchController = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];
    }
    
    searchController.mainController = self;
    currentMenuItemTag = SearchButtonTag;
    [self presentNewViewController:searchController];
}

-(void)presentQRcodeScannerController
{
   
    
    
    if (IOS_VERSION >= 8)
    {
        
        
        NSError *error;
        AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
        
        AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
        
        if (!input) {
            NSLog(@"%@", [error localizedDescription]);
            //return 0;
        }
        
        self.captureSession = [[AVCaptureSession alloc] init];
        [self.captureSession addInput:input];
        
        AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
        [self.captureSession addOutput:captureMetadataOutput];
        
        dispatch_queue_t dispatchQueue;
        dispatchQueue = dispatch_queue_create("myQueue", NULL);
        [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
        [captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
        
        self.videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
        [self.videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        [self.videoPreviewLayer setFrame:self.view.layer.bounds];
        //[self.view.layer addSublayer:videoPreviewLayer];
        
        
        
        
        qrSubView = [[UIView alloc] initWithFrame:self.view.layer.bounds];
        [qrSubView.layer addSublayer:  self.videoPreviewLayer];
        [self.view addSubview: qrSubView];
        
        
        
        UIImageView *overlayImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"overlaygraphic.png"]];
        [overlayImageView setFrame:CGRectMake(30, 100, 260, 200)];
        [ qrSubView addSubview:overlayImageView];
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityIndicator.alpha = 1.0;
        activityIndicator.center = CGPointMake(160, 200);
        activityIndicator.hidesWhenStopped = NO;
        [qrSubView addSubview:activityIndicator];
        [activityIndicator startAnimating];
        
        [captureSession startRunning];
        
        
        button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button addTarget:self
                   action:@selector(stopReading)
         forControlEvents:UIControlEventTouchUpInside];
        button.backgroundColor=[UIColor blackColor];
        [button setTitle:@"Cancel" forState:UIControlStateNormal];
        button.layer.cornerRadius=5.0;
        button.frame = CGRectMake(2.0, self.view.frame.size.height-42, 80.0, 40.0);
        [self.view addSubview:button];
        
    }
    else
    {

    
    // ADD: present a barcode reader that scans from the camera feed
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    UIImageView *overlayImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"overlaygraphic.png"]];
    [overlayImageView setFrame:CGRectMake(30, 100, 260, 200)];
    [reader.view addSubview:overlayImageView];
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.alpha = 1.0;
    activityIndicator.center = CGPointMake(160, 200);
    activityIndicator.hidesWhenStopped = NO;
    [reader.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    
    // present and release the controller
    [self presentModalViewController: reader
                            animated: YES];
    
    }
    
}


-(void)stopReading
{
    [self.captureSession stopRunning];
    
    self.captureSession = nil;
    [self.videoPreviewLayer removeFromSuperlayer];
    [button removeFromSuperview];
    [qrSubView removeFromSuperview];
    
}
-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    if (metadataObjects != nil && [metadataObjects count] > 0) {
        
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode])
        {
            //            [self.statusLabel performSelectorOnMainThread:@selector(setText:) withObject:[metadataObj stringValue] waitUntilDone:NO];
            //NSURL *url = [NSURL URLWithString:[metadataObj stringValue]];
            NSString *url=[metadataObj stringValue];
            if (url)
                
                [self performSelectorOnMainThread:@selector(goToURL:) withObject:url waitUntilDone:NO];
            
            [self performSelectorOnMainThread:@selector(stopReading) withObject:nil waitUntilDone:NO];
            //[self.startButton performSelectorOnMainThread:@selector(setTitle:) withObject:@"Start!" waitUntilDone:NO];
            isReading = NO;
        }
    }
}
-(void)goToURL:(NSString *)url
{
    
    NSString *testString = url;
    NSDataDetector *detect = [[NSDataDetector alloc] initWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [detect matchesInString:testString options:0 range:NSMakeRange(0, [testString length])];
    //Handle URL...
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]]];
    NSLog(@"URL is uhgjgh is %@",url);
    
    NSString *testString1 = url;
    NSDataDetector *detect1 = [[NSDataDetector alloc] initWithTypes:NSTextCheckingTypePhoneNumber error:nil];
    NSArray *matches1 = [detect1 matchesInString:testString1 options:0 range:NSMakeRange(0, [testString1 length])];
    
    NSLog(@"Result is %@", matches1);
    
    // NSLog(@"Result is %@",symbol.data);
    
    NSString *searchString=url;
    NSString *urlAddress = [NSString stringWithFormat:@"http://www.google.com/search?q=%@",searchString];
    NSURL *url2 = [NSURL URLWithString:[urlAddress stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    [[UIApplication sharedApplication] openURL:url2];
    
    [self stopReading];
}


- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    
    // ADD: dismiss the controller (NB dismiss from the *reader*!)
    if([symbol.typeName isEqualToString:@"QR-Code"])
    {
        
        NSString *testString = symbol.data;
        NSDataDetector *detect = [[NSDataDetector alloc] initWithTypes:NSTextCheckingTypeLink error:nil];
        NSArray *matches = [detect matchesInString:testString options:0 range:NSMakeRange(0, [testString length])];
        
        NSLog(@"Result is %@", matches);
        NSURL *url = [NSURL URLWithString:symbol.data];
        
        [[UIApplication sharedApplication] openURL:url];
        
        
        NSString *testString1 = symbol.data;
        NSDataDetector *detect1 = [[NSDataDetector alloc] initWithTypes:NSTextCheckingTypePhoneNumber error:nil];
        NSArray *matches1 = [detect1 matchesInString:testString1 options:0 range:NSMakeRange(0, [testString1 length])];
        
        NSLog(@"Result is %@", matches1);
        
        NSLog(@"Result is %@",symbol.data);
        
        NSString *searchString=symbol.data;
        NSString *urlAddress = [NSString stringWithFormat:@"http://www.google.com/search?q=%@",searchString];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlAddress]];
        
        
        
        
        
    }
    [reader dismissModalViewControllerAnimated: YES];
}


- (void) presentNearByProductsController {
    
    if (!nearByViewController) {
        appDelegate.isNoticeBoardBground = NO;
        nearByViewController = [[NearByViewController alloc] initWithNibName:@"NearByViewController" bundle:nil proArray:nil];
    }
    
    [self presentNewViewController:nearByViewController];
    
}

- (void) presentMyfavoritesProductsController {
    
    if (!myFavoritesViewController) {
        appDelegate.isNoticeBoardBground = NO;
        myFavoritesViewController = [[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:nil productListType:ProductListTypeMyFavorites];
    }
    
    [self presentNewViewController:myFavoritesViewController];
}

- (void) presentParkingTimerController
{
    if (!ParkingtimerViewController) {
        appDelegate.isNoticeBoardBground = NO;
        ParkingtimerViewController = [[MyParkerTimerViewController alloc]initWithNibName:@"MyParkerTimerViewController" bundle:nil];
    }
    
    [self presentNewViewController:ParkingtimerViewController];
}

- (void) presentOfferProductsController {
    
     if(!loyaltyViewController)
    {
    appDelegate.isNoticeBoardBground = NO;
//    offersViewController = [[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:nil productListType:ProductListTypeOffers];
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
    loyaltyViewController = [storyboard instantiateViewControllerWithIdentifier:@"loyaltyViewController"];
    
    
//    [self presentNewViewController:offersViewController];
    }
    [self presentNewViewController:loyaltyViewController];
}

- (void) presentFreebiesViewController {
    if (!freebiesViewController) {
        appDelegate.isNoticeBoardBground = NO;
                UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
        freebiesViewController = [storyboard instantiateViewControllerWithIdentifier:@"freebiesViewController"];
    
    }
    [self presentNewViewController:freebiesViewController];
}

- (void) presentHelpPageController {
    
    if (!helpPageController) {
        appDelegate.isNoticeBoardBground = NO;
        helpPageController = [[HelpViewController alloc] initWithNibName:@"HelpViewController" bundle:nil];
    }
    
    [self presentNewViewController:helpPageController];
    
}

- (void) presentMyCardController {
    
    if(!myCardViewController)
    {
        appDelegate.isNoticeBoardBground = NO;
        myCardViewController = [[MyCardViewController alloc] initWithNibName:@"MyCardViewController" bundle:nil];
    }
    
    [self presentNewViewController:myCardViewController];
}

- (void) presentdialyDealController {
    
    if(!dailyDealController)
    {
        appDelegate.isNoticeBoardBground = NO;
        dailyDealController = [[DailyDealsViewController alloc] initWithNibName:@"DailyDealsViewController" bundle:nil];
    }
    
    [self presentNewViewController:dailyDealController];
}

- (void) presentNoticeBoardController {
    
//    if(!noticeViewController)
//    {
    appDelegate.isNoticeBoardBground = YES;
    noticeViewController = [[NoticeBoardViewController alloc] initWithNibName:@"NoticeBoardViewController" bundle:nil];
//    }
    [appDelegate.refArray removeAllObjects];
    [appDelegate.refArray addObject:noticeViewController];
    [self presentNewViewController:noticeViewController];
}

-(void)presentSendAFriendController{
    
    appDelegate.isNoticeBoardBground = NO;
   // SendAFriendViewController *sendAFrndVC = [[SendAFriendViewController alloc]initWithNibName:@"SendAFriendViewController" bundle:nil];
    
    
  //  [self presentNewViewController:sendAFrndVC];
    
}

/*
 - (IBAction)menuItemButtonTapped:(id)sender {
 
 NSLog(@"menuItemButtonTapped");
 
 currentMenuItemTag = [sender tag];
 
 switch ([sender tag]) {
 case SearchButtonTag:
 [self presentSearchController];
 break;
 case NearByButtonTag:
 [self presentNearByProductsController];
 break;
 case MyFavoritesButtonTag:
 [self presentMyfavoritesProductsController];
 break;
 case HotOffersButtonTag:
 [self presentOfferProductsController];
 break;
 case MyCardButtonTag:
 [self presentMyCardController];
 break;
 case DailyDealButtonTag:
 [self presentdialyDealController];
 case NoticeBoardButtonTag:
 [self presentNoticeBoardController];
 case SendAFriendTag:
 [self presentSendAFriendController];
 
 
 default:
 break;
 }
 
 [self menuButtonTapped:menuButton];
 
 }
 */
-(void) fetchNoticeBoardItemsCount{
    
    // NSLog(@"... fetchNoticeBoardItemsCount ...");
    
    NSString *urlString = [NSString stringWithFormat:@"http://myrewards.com.au/newapp/get_notice_ids.php?cid=1"];
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIFormDataRequest *req = [[ASIFormDataRequest alloc] initWithURL:url];
    //  NSLog(@"NoticeBoardItemsCount url:%@",urlString);
    [req setDelegate:self];
    [req startAsynchronous];
    noticeBoardIDsRequest = req;
    
}
- (void) fetchBannerView {
    //get_client_banner.php
    
    NSLog(@"... fetchBannerView ...");
    
    NSString *urlString = [NSString stringWithFormat:@"%@get_client_banner.php?cid=%@",URL_Prefix,appDelegate.sessionUser.client_id];
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIFormDataRequest *req = [[ASIFormDataRequest alloc] initWithURL:url];
    //  NSLog(@"banner url:%@",urlString);
    //[req setPostValue:appDelegate.sessionUser.client_id forKey:@"cid"]; // pwd, sub
    [req setDelegate:self];
    [req startAsynchronous];
    bannerFetchRequest = req;
}

#pragma mark -- ASIHttpRequestDelegate methods

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    if (request == bannerFetchRequest) {
        
        bannerFetchRequest = nil;
        
        
        UIImage *image = [UIImage imageWithData:[request responseData]];
        //    NSLog(@"banner image size: %@",NSStringFromCGSize(image.size));
        bannerImageView.image = image;
        
    }
    
    if(request == noticeBoardIDsRequest){
        //NSLog(@"NB IDs:%@",[request responseString]);
        noticeBoardIDsRequest = nil;
        NSXMLParser *noticeParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        NoticeBoardXMLParser = [[NoticeBoardDataParser alloc] init];
        NoticeBoardXMLParser.delegate = self;
        noticeParser.delegate = NoticeBoardXMLParser;
        [noticeParser parse];
    }
    
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    
    if (request == bannerFetchRequest) {
        NSLog(@"bannerFetchRequest FAILED: %@",request.error.description);
        bannerFetchRequest = nil;
    }
    if(request == noticeBoardIDsRequest){
        NSLog(@"NoticesIDFetchRequest FAILED: %@",request.error.description);
        noticeBoardIDsRequest = nil;
    }
}


- (void)showMailComposer {
    
    if ([MFMailComposeViewController canSendMail]) {
        NSArray *receipntsArray = [[NSArray alloc]initWithObjects:@"support@therewardsteam.com",nil];
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setToRecipients:receipntsArray];
        [mailViewController setSubject:@"Help"];
        [mailViewController setMessageBody:@"Your message goes here." isHTML:NO];
        
        [self presentModalViewController:mailViewController animated:YES];
        
    }
    
    else {
        
        NSLog(@"\nDevice is unable to send email in its current state.");
        UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                             message:@"You will need to setup a mail account on your device before you can send mail!"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [alert_view show];
        
        
    }
    
    
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    [self dismissModalViewControllerAnimated:YES];
    
}

#pragma mark -- UITableView datasource & delegate methods

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 1;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    /* if (section == 0) {
     return 9;
     }
     else if (section == 1) {
     return 2;
     }
     
     return 0;  */
    return [menuItemsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *reusableIdentifier = @"cell";
    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
    
    MenuTableCell *cell = (MenuTableCell *)[tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
    
    if (!cell) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableIdentifier];
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"MenuTableCell" owner:nil options:nil];
        
        for (UIView *cellview in views)
        {
            if([cellview isKindOfClass:[UITableViewCell class]])
            {
                cell = (MenuTableCell*)cellview;
            }
        }

    }
    
    // Add white line at the bottom of the cell
    float cellHeight = [self tableView:menuTable heightForRowAtIndexPath:indexPath];
    float whiteLineHeight = 1.0;
    float whiteLineX = 0.0;
    float whiteLineY = cellHeight - whiteLineHeight;
    float whiteLineWidth = tableView.frame.size.width;// - whiteLineX - (0.20 * tableView.frame.size.width);
    CGRect whiteLineFrame = CGRectMake(whiteLineX, whiteLineY, whiteLineWidth, whiteLineHeight);
    UIView *whiteLine = [[UIView alloc] initWithFrame:whiteLineFrame];
    whiteLine.alpha = 0.6;
    whiteLine.backgroundColor = [UIColor lightGrayColor];
    
    
    
    
    
    
    // Do not pput a line in the last cell of section
    if (indexPath.row != ([self tableView:menuTable numberOfRowsInSection:indexPath.section] - 1)) {
        [cell.contentView addSubview:whiteLine];
    }
    
    // Set Titles
    //if (indexPath.section == 0) {
//    cell.textLabel.textAlignment = NSTextAlignmentCenter;
   
    cell.menuItemName.text = [menuItemsArray objectAtIndex:indexPath.row];
    // cell.textLabel.font = [UIFont fontWithName:@"ArialRoundedMTBold" size:27];
    
//    [cell.textLabel setFont:[UIFont fontWithName:@"MyriadPro-Semibold" size:21]];
    cell.menuItemImage.image = [UIImage imageNamed:[menuItemsImageNamesArray objectAtIndex:indexPath.row]];
       
    
    if(indexPath.row == 6){
        
        if(badgeView)
        {
            NSLog(@"Removing badge View");
            badgeView = nil; [[cell.contentView viewWithTag:1] removeFromSuperview];
        }
        
        if(NCount!=0)
        {
            NSLog(@"Creating badge View");
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
                badgeView = [[UIView alloc]initWithFrame:CGRectMake(220, 3, 28, 28)];
            else
                badgeView = [[UIView alloc]initWithFrame:CGRectMake(220, 3, 28, 28)];
            
            badgeView.tag = 1;
            badgeView.backgroundColor = [UIColor clearColor];
            [badgeView addSubview:customBadge];
            [cell.contentView addSubview:badgeView];
        }
        
    }
    // }
    /*    else if (indexPath.section == 1){
     int section0Items = [self tableView:menuTable numberOfRowsInSection:0];
     int index = indexPath.row + section0Items;
     cell.textLabel.text = [menuItemsArray objectAtIndex:index];
     cell.imageView.image = [UIImage imageNamed:[menuItemsImageNamesArray objectAtIndex:index]];
     }  */
    
    
    
    if ( indexPath.row == currentSelectedIndexPath.row ) {
        cell.menuItemName.textColor = [UIColor blueColor]; //[UIColor colorWithRed:31.0/255 green:153.0/255 blue:212.0/255 alpha:1.0];
    }
    else {
        
        cell.menuItemName.textColor = [UIColor blackColor];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    //cell.textLabel.font = [UIFont boldSystemFontOfSize:20];
    cell.textLabel.font = [UIFont fontWithName:@"MyriadPro-Light" size:(20.0)];
    cell.backgroundColor = [UIColor clearColor];
    
}
// ** Used this method in viewForHeaderInSection method.
// ** Do not remove this method.
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 140.0;
}

- (UIImageView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIImageView *nwimgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,tableView.bounds.size.width , 150)];
    nwimgView.image=[UIImage imageNamed:@"background"];
    
    UIImageView *logImgView = [[UIImageView alloc] initWithFrame:CGRectMake(tableView.bounds.size.width/3, 10, 90, 90)];
    logImgView.image=[UIImage imageNamed:@"mrlogo.png"];
    [nwimgView addSubview:logImgView];
    
    UIImageView *nameImgView = [[UIImageView alloc] initWithFrame:CGRectMake(tableView.bounds.size.width/3.5, logImgView.bounds.size.height+20, 133, 25)];
    nameImgView.image=[UIImage imageNamed:@"mr-txt.png"];
    [nwimgView addSubview:nameImgView];
    
    return nwimgView;
}

// ** Used this method in cellForRow method.
// ** Do not remove this method.
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //    if (indexPath.section == 0) {
    //        if (indexPath.row == 5 || indexPath.row == 6) {
    //            return 0;
    //        }
    //    }
    
    return 50.0;
}

// Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath == currentSelectedIndexPath) {
        throughTableCellPressed = YES;
        [self menuButtonTapped:menuButton];
        return;
    }
    
    if (indexPath.row == 6)
        throughTableCellPressed = YES;
    // First dismiss menu view
    [self menuButtonTapped:menuButton];
    
    
    // Process tap on a menu item
    currentSelectedIndexPath = indexPath;
    [tableView reloadData];
    
    
    if (indexPath.row == 0) {
        //[self presentSearchController];
        [self performSelector:@selector(presentSearchController) withObject:nil afterDelay:0.3];
    }
    else if (indexPath.row == 1) {
        //[self presentNearByProductsController];
        [self performSelector:@selector(presentNearByProductsController) withObject:nil afterDelay:0.3];
    }
    else if (indexPath.row == 2) {
        //[self presentOfferProductsController];
        [self performSelector:@selector(presentOfferProductsController) withObject:nil afterDelay:0.3];
    }
    else if (indexPath.row == 3) {
        //[self presentMyfavoritesProductsController];
        [self performSelector:@selector(presentMyfavoritesProductsController) withObject:nil afterDelay:0.3];
    }
    else if (indexPath.row == 4) {
        // Present Daily Deal
        [self performSelector:@selector(presentdialyDealController) withObject:nil afterDelay:0.3];
    }
    else if (indexPath.row == 5) {
        
        // Present QRcode
        
        [self performSelector:@selector(presentQRcodeScannerController) withObject:nil afterDelay:0.3];
    }
    else if (indexPath.row == 6) {
        // Present Card
//        [self performSelector:@selector(presentMyCardController) withObject:nil afterDelay:0.3];
        
        
        // Present Notice Board
        //throughTableCellPressed = YES;
        NSLog(@"cell throughTableCellPressed::%d",throughTableCellPressed);
        [self performSelector:@selector(presentNoticeBoardController) withObject:nil afterDelay:0.3];
    }
    else if (indexPath.row == 7) {
        // Present  parkingtimer
//        [self performSelector:@selector(presentParkingTimerController) withObject:nil afterDelay:0.3];
        
        //Freebies
        
        [self performSelector:@selector(presentFreebiesViewController) withObject:nil afterDelay:0.3];
        
    }
    else if (indexPath.row == 8) {
        // Logout session
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Logout?" message:@"Do you really want to logout?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        alert.tag = 21221;
        [alert show];
        
    }
    else if (indexPath.row == 9) {
        // Present faceBook
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://m.facebook.com/myrewardsinternational"]];
        
    }
    else if (indexPath.row == 10) {
        
        // Present Twitter
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://mobile.twitter.com/MyRewardsIntl"]];
        
        
    }
    else if (indexPath.row == 11)
    {// Present  Help
        
        [self performSelector:@selector(presentHelpPageController) withObject:nil afterDelay:0.3];
        
    }
    else
    {
        
       
        
    }
    
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 21221) {
        if (buttonIndex == alertView.cancelButtonIndex) {
            [appDelegate logoutUserSession];
        }
    }
    
}

//============================ ***********************   ==========================//

#pragma mark -- Notice Board Data Parser Delegate methods

- (void) parsingNoticeBoardDataFinished:(NSArray *) noticesList
{
    
    if (!noticeIDArray) {
        noticeIDArray = [[NSMutableArray alloc] initWithArray:noticesList];
    }
    else {
        [noticeIDArray removeAllObjects];
        [noticeIDArray addObjectsFromArray:noticesList];
    }
    
    
    
    NSLog(@" webservice noticeIDs Count: %lu",(unsigned long)[noticeIDArray count]);
    
    [self calculatingUnreadNotices];
    
    
    
}




-(void) calculatingUnreadNotices{
    
    // NSLog(@"calculatingUnreadNotices");
    
    NoticeBoard *currrentNotice;
    NoticeBoard *cur;
    NCount = 0;
    
    NSMutableArray *myArray = [[NSMutableArray alloc]init];
    [myArray addObjectsFromArray:[appDelegate noticeBoardIds]];
    
    if([myArray count] >0 )
    {
        
        for (int i = 0; i<[noticeIDArray count]; i++)
        {
            
            for (int j=0; j<[myArray count]; j++)
            {
                
                currrentNotice = [noticeIDArray objectAtIndex:i];
                cur = [myArray objectAtIndex:j];
                
                if([currrentNotice.notice_Id isEqualToString:cur.notice_Id])
                    NCount++;
                
                
            }
            
        }
        NCount  = [noticeIDArray count]- NCount;
    }
    else
        NCount  = [noticeIDArray count]- NCount;
    
    
    noticeBoardCount = [NSString stringWithFormat:@"%d",NCount];
    // NSLog(@"noticeBoardCount:%@",noticeBoardCount);
    appDelegate.badgecount = [noticeBoardCount intValue];
    NSLog(@"Unread count:: %d",NCount);
    customBadge = nil;
    if (NCount!=0) {
        
        
        customBadge = [CustomBadge customBadgeWithString:noticeBoardCount
                                         withStringColor:[UIColor whiteColor]
                                          withInsetColor:[UIColor blueColor]
                                          withBadgeFrame:YES
                                     withBadgeFrameColor:[UIColor whiteColor]
                                               withScale:1.0
                                             withShining:YES];
        
    }
    [self.menuTable reloadData];
    
}


- (void) parsingNoticeBoardDataXMLFailed
{
    
}



@end

/*
 0,-99,320,200
 */
