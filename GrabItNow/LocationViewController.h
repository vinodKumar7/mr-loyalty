//
//  LocationViewController.h
//  TWU
//
//  Created by vairat on 20/06/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParkingTimer.h"


@interface LocationViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *where_Label;
@property (strong, nonatomic) IBOutlet UILabel *when_Label;
@property (strong, nonatomic) IBOutlet UILabel *latitude_Label;
@property (strong, nonatomic) IBOutlet UILabel *longitude_Label;
@property (strong, nonatomic) IBOutlet UILabel *parkedlocationlabel;
@property (strong, nonatomic) IBOutlet UILabel *wherefixedLabel;
@property (strong, nonatomic) IBOutlet UILabel *whenfixedLabel;
@property (strong, nonatomic) IBOutlet UILabel *latitudefixedLabel;
@property (strong, nonatomic) IBOutlet UILabel *longitudefixedLabel;
@property (strong, nonatomic) IBOutlet UIButton *getDirectionsButton;
@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil userLoc:(ParkingTimer *) locDetails carLoc:(ParkingTimer *)carDetails;

- (IBAction)directions_Action:(id)sender;
@end
