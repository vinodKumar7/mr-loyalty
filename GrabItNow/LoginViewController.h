//
//  LoginViewController.h
//  GrabItNow
//
//  Created by vairat on 29/04/15.
//  Copyright (c) 2015 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "UserDataXMLParser.h"
#import "CustomIOS7AlertView.h"
#import "Facebook.h"
#import "FBRequestWrapper.h"
#import "FBRequest.h"


@interface LoginViewController : UIViewController <UITextFieldDelegate, ASIHTTPRequestDelegate, NSXMLParserDelegate, UserDataXMLParser,UIActionSheetDelegate, UIPickerViewDelegate,UIPickerViewDataSource, CustomIOS7AlertViewDelegate, FBSessionDelegate,FBRequestDelegate>
{
    BOOL processRenwalOfToken;
    
    Facebook *facebook;
    FBRequestWrapper *FRwrapper;
    NSString * facebookid;
    NSString * facebookname;
    NSMutableDictionary *facebookdictionary;
}

@property (strong, nonatomic) IBOutlet UIPageControl *coarouselPage_Control;
@property (strong, nonatomic) IBOutlet UIImageView *screens_ImageView;

- (UIViewController *) controllerAtIndex:(NSInteger) index ;

@end
